import {Component, OnInit} from '@angular/core';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedHttpService} from '../services/SharedHttp.service';
import {DexieService} from '../services/dexie.service';
import * as $ from 'jquery';
import {User} from '../models/User';
import {StorageService} from "../services/storage.service";
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-new-product',
    templateUrl: './new-product.component.html',
    styles: []
})
export class NewProductComponent implements OnInit {

    id;
    productSchema;
    public isOnline = true;
    public company = [];
    public tags = [];
    public products = [];
    public productToEdit;
    public filteredTags = [];
    public tagNames = [];
    public categories = [];
    public form: FormGroup;
    public responseType = 'd-none';
    public FormErr: String = '';
    public FormState: String = '';
    public ModalState = false;
    public editId = null;
    public CompanyBarcode;
    public exitAfterSave = true;
    public selectedTags = [];
    public currentUser: User;
    files;
    links = [];
    localFileUrls = [];
    private subscription: Subscription;

    constructor(public dexieService: DexieService, private sanitizer: DomSanitizer, private transporter: TransporterService,
                private activatedRoute: ActivatedRoute, private fb: FormBuilder, private httpService: SharedHttpService,
                private storageService: StorageService, private router: Router) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'company') {
                this.company = parcel.data;
            }
            if (parcel.type === 'products') {
                this.products = parcel.data;
            }
            if (parcel.type === 'new-product') {
                if (parcel.data === 'start') {
                    $('body').removeClass('overflow-hidden');
                    this.ToggleModal();
                }
            }
            if (parcel.type === 'edit-product') {
                if (parcel.data) {
                    $('body').removeClass('overflow-hidden');
                    this.editId = parcel.data;
                    this.ToggleModal();
                    this.GetProduct();
                }
            }
        });
    }


    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.id = this.currentUser.Company;
        this.dexieGetTaxonomies();
        this.getUpdatedTaxonomies();
        this.form = this.fb.group({
            ProductType: ['Single', Validators.required],
            Name: ['', Validators.required],
            Price: ['', Validators.required],
            Company: [this.currentUser['Company']],
            Approved: [this.currentUser['AccessLevel'] === 'SuperAdmin'],
            Category: [''],
            Barcode: ['', this.barcodeCheck.bind(this)],
            CustomProperties: []
        });
        this.jqueryLogic();

        this.httpService.Get('/companies/' + this.id).subscribe(data => {
            this.company = data['body'];

            this.productSchema = data['body']['ProductSchema'];

            if (this.productSchema) {
                let schemaObject = this.fb.array([]);
                this.productSchema.forEach(p => {
                    schemaObject.push(
                        this.fb.group({
                                Name: [p.Name, [Validators.required]],
                                Value: ['', [Validators.required]],
                                Type: [p.Type, [Validators.required]],
                            }
                        )
                    );
                    this.form = this.fb.group({
                        ProductType: ['Single', Validators.required],
                        Name: ['', Validators.required],
                        Price: ['', Validators.required],
                        Company: [this.currentUser['Company']],
                        Approved: [this.currentUser['AccessLevel'] === 'SuperAdmin'],
                        Category: [''],
                        Barcode: ['', this.barcodeCheck.bind(this)],
                        CustomProperties: schemaObject
                    });
                });
            }
            console.log('COMPANY', this.company);
        }, err => {
        });
    }

    getProductProperties(): FormArray {
        return <FormArray>this.form.get('CustomProperties');
    }


    resetForm() {
        this.selectedTags = [];
        this.CompanyBarcode = '';
        this.form.reset({
            ProductType: 'Single',
            Barcode: '',
            Name: '',
            Price: 1,
            Company: this.currentUser['Company'],
            Approved: 0,
            Category: '',
        });
        this.form.get('ProductType').setValidators(Validators.required);
        this.form.get('Name').setValidators(Validators.required);
        this.form.get('Price').setValidators(Validators.required);
        this.form.get('Barcode').setValidators(this.barcodeCheck);
        if (this.exitAfterSave) {
            this.ToggleModal();
        }
        this.links = [];
        this.localFileUrls = [];
        this.files = [];
        this.editId = undefined;
    }

    GetProduct() {
        this.httpService.Get('/products/' + this.editId)
            .subscribe(
                res => {
                    this.productToEdit = res['body'];
                    console.log('edit val', this.productToEdit);
                    let schemaObject = this.fb.array([]);

                    if (this.productSchema) {
                        this.productSchema.forEach(p => {
                            let value = '';
                            const refToCustomProperty = this.productToEdit.CustomProperties.find(x => x.Name === p.Name);
                            if (refToCustomProperty) {
                                value = refToCustomProperty.Value;
                            }

                            schemaObject.push(
                                this.fb.group({
                                        Name: [p.Name],
                                        Value: [value],
                                        Type: [p.Type],
                                    }
                                )
                            );

                        });
                    }

                    this.form = this.fb.group({
                        ProductType: this.productToEdit.ProductType ? this.productToEdit.ProductType : 'Single',
                        Barcode: this.productToEdit.Barcode ? this.productToEdit.Barcode : '',
                        Name: this.productToEdit.Name ? this.productToEdit.Name : '',
                        Price: this.productToEdit.Price ? this.productToEdit.Price : 1,
                        Company: this.currentUser['Company'],
                        Approved: this.productToEdit.Approved,
                        Category: this.productToEdit.Category ? this.productToEdit.Category : '',
                        CustomProperties: schemaObject
                    });

                    console.log(this.form.value);
                    // images
                    this.productToEdit.Images.forEach(img => {
                        const myFile = {loading: false, path: img};
                        this.localFileUrls.push(myFile);
                    });
                    this.links = this.productToEdit.Images;
                },
                err => {
                    if (err.error && err.status !== 404) {
                        console.log('Unexpected error');
                    } else {
                        this.productToEdit = null;
                    }
                }
            );
    }


    jqueryLogic() {
        const $this = this;
        $('body').on('click', function (e) {
            if ($(e.target).closest('.custom-tag-selector').length === 0) {
                $this.filteredTags = [];
            }
        });
    }

    barcodeCheck(control: AbstractControl): ValidationErrors | null {
        if (control.value.length && this.products) {
            for (let i = 0; i < this.products.length; i++) {
                if (this.products[i].Barcode === control.value && this.products[i]._id !== this.editId) {
                    return {error: true};
                }
            }
            return null;
        } else {
            return null;
        }
    }

    checkForNumber(e) {
        if (e.target.value.length) {
            const val = +e.target.value;
            if (!val) {
                e.target.value = 1;
            }
        }
    }

    dexieGetTaxonomies() {
        this.dexieService['Taxonomy'].where({Company: this.currentUser['Company']}).toArray().then(data => {
            this.tags = data.filter(obj => {
                if (!obj.IsCategory) {
                    return obj;
                }
            });
            this.categories = data.filter(obj => {
                if (obj.IsCategory) {
                    return obj;
                }
            });
        }).catch(err => {
            console.log(err);
            console.log('err in taxonomies record.');
        });
    }

    getUpdatedTaxonomies() {
        let lastUpdated;
        if (this.isOnline) {
            this.dexieService['Flag'].where({Name: 'Taxonomy'}).toArray().then(data => {
                lastUpdated = data[0];
                this.GetTaxonomies(lastUpdated['Value']);
            }).catch(err => {
                this.GetTaxonomies(false);
            });
        }
    }

    GetTaxonomies(updatedAt: any) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }
        this.httpService.Get('/taxonomies?company=' + this.currentUser['Company'] + queryPart)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.dexieService['Taxonomy'].bulkPut(res['body']['data']).then(data => {
                            console.log('Taxonomies synced');
                            this.dexieService['Flag'].put({
                                Name: 'Taxonomy',
                                Value: new Date().toJSON()
                            }).catch(flagErr => {
                                console.log('Failed to put Taxonomy flag ' + flagErr);
                            });
                            this.dexieGetTaxonomies();
                        }).catch(syncErr => {
                            console.log('Failed to put resulted company: ' + syncErr);
                        });
                    } else {
                        console.log('Taxonomies are up-to-date');
                    }
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error with status ' + err.status);
                    }
                }
            );
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    closeModal() {
        this.exitAfterSave = false;
        console.log(this.resetForm());
        console.log(this.ToggleModal());
        this.exitAfterSave = true;

    }


    PostProduct() {
        this.httpService.Post('/products', this.form.value)
            .subscribe(
                data => {
                    console.log(data);
                    this.FormState = '';
                    this.responseType = 'success';
                    this.FormErr = 'Product added successfully.';
                    this.transporter.transmit({type: 'get-products', data: true});
                    setTimeout(() => {
                        this.responseType = 'd-none';
                    }, 3000);
                    this.resetForm();
                },
                err => {
                    if (err.error) {
                        const data = err.error.errors;
                        this.FormState = '';
                        this.responseType = 'error';
                        for (const key of Object.keys(data)) {
                            if (key !== 'Company') {
                                if (data[key].kind === 'required') {
                                    this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                                } else {
                                    if (key === 'CompanyBarcode') {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                                    } else {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                                    }
                                }
                            }
                        }
                        setTimeout(() => {
                            this.responseType = 'd-none';
                        }, 4000);
                        this.form.reset(this.form.value);
                    }
                }
            );
    }

    PatchProduct() {
        console.log(this.form.value);
       this.httpService.Patch('/products/' + this.editId, this.form.value)
           .subscribe(
               data => {
                   console.log(data);
                   this.FormState = '';
                   this.responseType = 'success';
                   this.FormErr = 'Product Updated successfully.';
                   this.transporter.transmit({type: 'get-products', data: true});
                   setTimeout(() => {
                       this.responseType = 'd-none';
                   }, 3000);
                   this.resetForm();
               },
               err => {
                   if (err.error) {
                       const data = err.error.errors;
                       this.FormState = '';
                       this.responseType = 'error';
                       for (const key of Object.keys(data)) {
                           if (key !== 'Company') {
                               if (data[key].kind === 'required') {
                                   this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                               } else {
                                   if (key === 'CompanyBarcode') {
                                       this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                                   } else {
                                       this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                                   }
                               }
                           }
                       }
                       setTimeout(() => {
                           this.responseType = 'd-none';
                       }, 4000);
                       this.form.reset(this.form.value);
                   }
               }
           );
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }


    async onFileChange(event) {
        const page = this;
        this.files = event.target.files;
        console.log(this.files);
        console.log(this.files[0].name);
        // this.links = [];
        // this.localFileUrls = [];
        $(".product-images").append('');
        for (let index = 0; index < this.files.length; index++) {
            var path = URL.createObjectURL(event.target.files[index]);
            console.log('path', path);
            const myFile = {loading: true, path: path};
            this.localFileUrls.push(myFile);
            const file = this.files[index];
            this.storageService.addFile(file, path, (link, xpath) => {
                page.links.push(link);
                console.log('FILE', xpath);
                console.log('URLS', page.localFileUrls);
                this.localFileUrls.find(x => x.path === xpath).loading = false;
                console.log('URLS XXXxxxXXX', page.localFileUrls);

                // todo: use image from here , (page.)
            });
        }
    }

    onImageDelete(event, index: any) {
        const page = this;
        this.storageService.deleteFile(this.links[index], () => {
            page.localFileUrls.splice(index, 1);
            page.links.splice(index, 1);
            if (this.editId) {
                this.form.value.Images = this.productToEdit.Images = this.links;
                this.httpService.Patch('/products/' + this.editId, {Images: this.links}).subscribe(data => {
                }, err => {
                })
            }
        });
    }

    submit(exit) {
        this.exitAfterSave = exit;
        if (this.form.valid) {
            if (this.links.length) {
                this.form.value.Images = this.links;
            }
            this.FormState = 'loading';
            if (this.selectedTags.length) {
                this.form.value.Tags = this.selectedTags;
            }
            if (this.form.value.Barcode && this.form.value.Barcode.trim().length) {
                this.form.value.CompanyBarcode = this.currentUser.Company + this.form.value.Barcode;
            } else {
                delete this.form.value.Barcode;
            }
            if (!this.editId) {
                this.PostProduct();
            } else {
                this.PatchProduct();
            }

        } else {
            console.log('form value', this.form.value);
            console.log('not valid');
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control.markAsDirty({onlySelf: true});
            });
        }
    }

    filterTags(e) {
        const val = e.target.value;
        this.filteredTags = this.tags.filter(tag => {
            return tag.Name.toLowerCase().indexOf(val) !== -1 || !val;
        });
    }

    selectTag(tag) {
        if (this.selectedTags.indexOf(tag._id) === -1) {
            this.selectedTags.push(tag._id);
            this.tagNames[tag._id] = tag.Name;
        }
        this.filteredTags = [];
        $('#searchTags').val('');
    }

    deSelectTag(id) {
        this.selectedTags.forEach((item, index) => {
            if (item === id) {
                this.selectedTags.splice(index, 1);
            }
        });
    }


}


