import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAuditComponent } from './product-audit.component';

describe('ProductAuditComponent', () => {
  let component: ProductAuditComponent;
  let fixture: ComponentFixture<ProductAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
