import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {TransporterService} from "../../services/transporter.service";
import {ActivatedRoute} from "@angular/router";
import {SharedHttpService} from "../../services/SharedHttp.service";
import * as $ from "jquery";
import {FormBuilder, Validators} from "@angular/forms";
import {v1} from "uuid";

@Component({
    selector: 'app-product-audit',
    templateUrl: './product-audit.component.html',
    styles: []
})
export class ProductAuditComponent implements OnInit {

    product = {};
    ModalState = false;
    form;
    locationId
    currentUser
    auditReceipt = {}
    private subscription: Subscription;

    constructor(private fb: FormBuilder, private transporter: TransporterService, private activatedRoute: ActivatedRoute, private httpService: SharedHttpService) {
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });

        this.form = this.fb.group({
            newProductQuantity: ['', Validators.required]

        });
        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'audit-product') {
                this.product = parcel.data;
                this.ModalState = true;
                this.form.value.newProductQuantity = this.product['Attribute']['Stock'];
                console.log(this.product);

            }
        });
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    submit() {
        let correction = this.form.value.newProductQuantity - this.product['Attribute']['Stock'];

        if (correction >=0) {
            this.auditReceipt['TransactionType'] = 'PositiveCorrection';
        } else if (correction < 0) {
            correction = -1 * correction;
            this.auditReceipt['TransactionType'] = 'NegativeCorrection';
        }

        console.log(correction);
        const tempUser = (this.currentUser._id).substr(this.currentUser._id.length - 3);
        const tempUUID = (v1()).split('-')[0];
        this.auditReceipt['AmountPaid'] = 0;
        this.auditReceipt['Products'] = [{Product: this.product, Quantity: correction}];
        this.auditReceipt['Store'] = this.locationId;

        this.auditReceipt['SalesPerson'] = this.currentUser._id;
        this.auditReceipt['Company'] = this.currentUser.Company;
        this.auditReceipt['LocalId'] = tempUser + tempUUID;
        this.auditReceipt['Date'] = new Date();
        console.log(this.auditReceipt)
        this.httpService.Post('/receipts', this.auditReceipt).subscribe(data => {
            this.transporter.transmit({type: 'get-products-after-audit'});

            this.ToggleModal();

        }, err => console.log(err));


    }

}
