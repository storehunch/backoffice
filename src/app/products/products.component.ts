import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../models/User';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {SharedHttpService} from "../services/SharedHttp.service";
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styles: []
})
export class ProductsComponent implements OnInit {

    id;
    company;
    productSchema;
    rows = [];
    categories;
    tags;
    public products = [];
    public currentUser: User;
    public isOnline = true;
    public locationId: string;
    loadingIndicator = true;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private subscription: Subscription;

    constructor(private printService: PrintService, private transporter: TransporterService, private activatedRoute: ActivatedRoute, private httpService: SharedHttpService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }

            if (parcel.type === 'products') {
                this.products = parcel.data;
                this.rows = this.products;
                console.log('products', this.rows)
                this.loadingIndicator = false;
            }
        });

        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.id = this.currentUser.Company;

    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });
        this.getTaxonomies();


        this.httpService.Get('/companies/' + this.id).subscribe(data => {
            this.company = data['body'];
            this.productSchema = data['body']['ProductSchema'];
            console.log('COMPANY', this.company);
        }, err => {
        });
    }

    auditProduct(product) {

        if (this.currentUser.AccessLevel === 'SuperAdmin') {

            console.log('audit clicked');
            this.transporter.transmit({type: 'audit-product', data: product});
        } else {

            alert('Audits are allowed to Super Admins Only, Contact your Administrator to audit this product.');
        }

    }

    filterByCategory(event) {
        const categoryFilter = event.target.value;
        console.log('category filter', categoryFilter)

        if (categoryFilter.length < 1) {
            this.rows = this.products;
        }
        else {
            this.rows = this.products.filter(row => row['Category'] && row['Category']['_id'] === categoryFilter);

        }


        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        this.rows = this.rows.filter(function (d) {
            return d.Name.toLowerCase().indexOf(val) !== -1 || !val || (d.Barcode && d.Barcode.toLowerCase().indexOf(val) !== -1);
        });


        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getTaxonomies() {

        this.httpService.Get('/taxonomies?company=' + this.currentUser.Company + '&store=' + this.locationId)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.categories = res['body']['data'].filter(x => x.IsCategory);
                        this.tags = res['body']['data'].filter(x => !x.IsCategory);


                    } else {
                        this.categories = [];
                        this.tags = [];
                    }

                },
                err => {
                    console.log('Unexpected error, Please contact support.');
                }
            );
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    triggerImport() {
        this.transport({type: 'import', data: 'start'});
    }

    editProduct(pid) {
        $('body').removeClass('overflow-hidden');
        this.transport({type: 'edit-product', data: pid});
    }

    triggerAddProduct() {
        $('body').removeClass('overflow-hidden');
        this.transport({type: 'new-product', data: 'start'});
    }

    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }

    printReport() {
        let temp = this.clone(this.rows);

        const mappedRows = temp.map(function (item) {
            delete item['_id'];
            delete item['IsFeatured'];
            delete item['Description'];
            delete item['Approved'];
            item['Stock'] = item['Attribute']['Stock'];
            delete item['Attribute'];
            delete item['Tags'];
            delete item['SubItems'];
            delete item['Images'];
            delete item['CustomProperties'];
            delete item['Category'];
            delete item['Company'];
            delete item['updatedAt'];
            delete item['createdAt'];
            delete item['__v'];
            return item;
        });
        let costVal = 0;
        let priceVal = 0;
        mappedRows.forEach(x => {

            if (x.Cost) {
                if (!this.isObject(x.Cost) && !isNaN(x.Cost) && x['Stock']) {
                    costVal = costVal + (+x.Cost) * +x.Stock;
                }
            }
            if (x.Price && !isNaN(x.Price) && !this.isObject(x.Price) && x['Stock']) {
                priceVal = priceVal + (+x.Price) * +x.Stock;
            }


        });
        console.log('costVal', costVal)
        console.log('priceVale', priceVal)
        const totalCostValueHtml = '<tr><th>Cost Value</th><td>' + costVal + '</td></tr>'
        const totalSaleValueHtml = '<tr><th>Sale Value</th><td>' + priceVal + '</td></tr>'
        this.printService.printReport([mappedRows], 'Inventory Report', this.locationId, totalCostValueHtml + totalSaleValueHtml);
    }

    isObject(val) {
        return val instanceof Object;
    }
}

