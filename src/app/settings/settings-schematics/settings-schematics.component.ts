import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SharedHttpService} from '../../services/SharedHttp.service';

@Component({
    selector: 'app-settings-schematics',
    templateUrl: './settings-schematics.component.html',
    styles: []
})
export class SettingsSchematicsComponent implements OnInit {

    id;
    form:FormGroup;
    currentUser;
    company;
    productSchema;
    public responseType = 'd-none';
    public FormErr:String = '';
    public FormState:String = '';


    constructor(private fb:FormBuilder, private httpService:SharedHttpService) {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.id = this.currentUser.Company;

        this.form = this.fb.group({
            ProductSchema:this.fb.array([this.createProductProperty()]),
        });

    }

    ngOnInit() {
        this.httpService.Get('/companies/' + this.id).subscribe(data => {
            this.company = data['body'];
            this.productSchema = data['body']['ProductSchema'];
            console.log('COMPANY', this.company);
            console.log('productSchema', this.productSchema);

            let schemaObject = this.fb.array([]);
            this.productSchema.forEach(p => {
                schemaObject.push(
                    this.fb.group({
                            Name: [p.Name, [Validators.required]],
                            Type: [p.Type, [Validators.required]]
                        }

                    )
            );
            this.form = this.fb.group({
                ProductSchema:schemaObject
            });
            });

        }, err => {
        });
    }




    getProductProperties():FormArray {
        return <FormArray>this.form.get('ProductSchema');
    }

    addProductProperty():void {
        const props = this.form.get('ProductSchema') as FormArray;
        props.push(this.createProductProperty());
    }

    createProductProperty():FormGroup {
        return this.fb.group({
            Name: [''],
            Type: ['text']
        });
    }

    removeProductProperty($event, index) {
        console.log('test');
        const props = this.form.get('ProductSchema') as FormArray;
        props.removeAt(index);
    }

    submit() {
        this.httpService.Patch('/companies/' + this.id, this.form.value).subscribe(data => {
            this.FormState = '';
            this.responseType = 'success';
            this.FormErr = 'Schema added successfully.';
            setTimeout(() => {
                this.responseType = 'd-none';
            }, 3000);
        }, err => {
            alert(err);
        });
    }

}
