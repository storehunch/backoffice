import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsSchematicsComponent } from './settings-schematics.component';

describe('SettingsSchematicsComponent', () => {
  let component: SettingsSchematicsComponent;
  let fixture: ComponentFixture<SettingsSchematicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsSchematicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsSchematicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
