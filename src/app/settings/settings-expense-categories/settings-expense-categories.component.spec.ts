import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsExpenseCategoriesComponent } from './settings-expense-categories.component';

describe('SettingsExpenseCategoriesComponent', () => {
  let component: SettingsExpenseCategoriesComponent;
  let fixture: ComponentFixture<SettingsExpenseCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsExpenseCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsExpenseCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
