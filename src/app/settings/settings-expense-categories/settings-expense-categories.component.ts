import { Component, OnInit } from '@angular/core';
import {SharedHttpService} from "../../services/SharedHttp.service";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from "../../services/transporter.service";

@Component({
  selector: 'app-settings-expense-categories',
  templateUrl: './settings-expense-categories.component.html',
  styles: []
})
export class SettingsExpenseCategoriesComponent implements OnInit {

  public categories: any[] = [];
    public currentUser = [];
  public ModalState = false;
  public isEdit = false;
    categoryId;
    categoryName;
  public formGroup: FormGroup;
    public FormState: String = '';
    public responseType = 'd-none';
    public FormErr: String = '';
    private subscription: Subscription;



  constructor(private transporter: TransporterService,private fb: FormBuilder, private httpService: SharedHttpService) {
      this.currentUser = JSON.parse(localStorage.getItem('user'));
      console.log(this.currentUser);

      this.subscription = this.transporter.receive().subscribe(parcel => {
          if (parcel.type === 'get-expense-categories') {
              this.GetCategories();
          }
      });
  }

  ngOnInit() {



    this.GetCategories();

      this.formGroup = this.fb.group({
          Name: ['', Validators.required],
          Company: [this.currentUser['Company']],
      });
  }


    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    closeModal(){
        if(this.isEdit){
            this.isEdit = !this.isEdit;
        }
        this.formGroup.setValue({
            Name: '',
            Company: this.currentUser['Company']
        });
        this.ToggleModal();
    }

    GetCategories() {
        this.httpService.Get('/expense-categories?company='+this.currentUser['Company'])
            .subscribe(
                res => {
                    console.log('res',res);
                    this.categories = res['body']['data'];
                    console.log('categories',this.categories);
                },
                err => {
                    if (err.error && err.status !== 404) {
                        console.log('Unexpected error');
                    } else {
                        this.categories = null;
                    }
                }
            );
    }

    editCategory(id:any, name:any){
        this.categoryId = id;
        this.categoryName = name;
        this.isEdit = true;
        this.ToggleModal();

        this.formGroup.setValue({
            Name: name,
            Company: this.currentUser['Company']
        });
    }

    submit() {
        if(this.isEdit){
            this.httpService.Patch('/expense-categories/' + this.categoryId, this.formGroup.value)
                .subscribe(
                    data => {
                        this.GetCategories();
                        console.log(data);
                        this.FormState = '';
                        this.responseType = 'success';
                        this.FormErr = 'Category edit successfully.';
                        setTimeout(() => {
                            this.responseType = 'd-none';
                        }, 3000);
                        this.isEdit = false;
                        this.formGroup.setValue({
                            Name: '',
                            Company: this.currentUser['Company']
                        });
                        this.ToggleModal();
                    },
                    err => {
                        if (err.error) {
                            const data = err.error.errors;
                            this.FormState = '';
                            this.responseType = 'error';
                            for (const key of Object.keys(data)) {
                                if (key !== 'Company') {
                                    if (data[key].kind === 'required') {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                                    } else {
                                        if (key === 'CompanyBarcode') {
                                            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                                        } else {
                                            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                                        }
                                    }
                                }
                            }
                            setTimeout(() => {
                                this.responseType = 'd-none';
                            }, 4000);
                            this.formGroup.reset(this.formGroup.value);
                        }
                    }
                );
        }else{
            this.httpService.Post('/expense-categories', this.formGroup.value)
                .subscribe(
                    data => {
                        console.log(data);
                        this.GetCategories();
                        this.FormState = '';
                        this.responseType = 'success';
                        this.FormErr = 'Category added successfully.';
                        setTimeout(() => {
                            this.responseType = 'd-none';
                        }, 3000);
                        this.formGroup.setValue({
                            Name: '',
                            Company: this.currentUser['Company']
                        });
                        this.ToggleModal();
                    },
                    err => {
                        if (err.error) {
                            const data = err.error.errors;
                            this.FormState = '';
                            this.responseType = 'error';
                            for (const key of Object.keys(data)) {
                                if (key !== 'Company') {
                                    if (data[key].kind === 'required') {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                                    } else {
                                        if (key === 'CompanyBarcode') {
                                            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                                        } else {
                                            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                                        }
                                    }
                                }
                            }
                            setTimeout(() => {
                                this.responseType = 'd-none';
                            }, 4000);
                            this.formGroup.reset(this.formGroup.value);
                        }
                    }
                );
        }

    }

}
