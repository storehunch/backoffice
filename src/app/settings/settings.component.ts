import {Component, OnInit} from '@angular/core';
import {TransporterService} from "../services/transporter.service";
import {SharedHttpService} from "../services/SharedHttp.service";
import {User} from '../models/AllModels';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styles: []
})
export class SettingsComponent implements OnInit {
    index = 0;
    companyData: any;
    public currentUser: User;
    settingsFilter = [true, false, false, false, false]; // 0 Profit Loss Statement, 1 Date Wise Sales Journal , 2 Category Wise Sales Report, 3 Cumilative Stock Sales Report,4 Product Summary
    private subscription: Subscription;


    constructor(private transporter: TransporterService, private httpService: SharedHttpService) {
        this.currentUser = JSON.parse(localStorage.getItem('user'));

        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'refresh-loyalty-program') {
                this.getLoyaltyProgram();
            }
        });
    }

    ngOnInit() {
        this.getLoyaltyProgram();
    }


    getLoyaltyProgram() {
        this.httpService.Get('/companies/' + this.currentUser.Company).subscribe(data => {
            console.log('company data', data);
            this.companyData = data['body'];
            this.transporter.transmit({type: 'get-company-data', data: this.companyData});
        }, err => console.log(err));

    }


    settingsChanged(index: number) { // index must be number so as not to mess with array indexes

        if (index === undefined) {
            return;
        } // keep this
        if (index === 0 || index === 1) {
            this.getLoyaltyProgram();
        }
        if (index === 3) {
            this.transporter.transmit({type: 'get-expense-categories', data: true});
        }
        this.settingsFilter = [false, false, false, false, false];
        this.settingsFilter[index] = true;
    }

}
