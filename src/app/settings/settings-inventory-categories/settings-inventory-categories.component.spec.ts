import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsInventoryCategoriesComponent } from './settings-inventory-categories.component';

describe('SettingsInventoryCategoriesComponent', () => {
  let component: SettingsInventoryCategoriesComponent;
  let fixture: ComponentFixture<SettingsInventoryCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsInventoryCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsInventoryCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
