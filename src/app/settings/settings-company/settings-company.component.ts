import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store, User} from '../../models/AllModels';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../../services/transporter.service';
import {StorageService} from "../../services/storage.service";
import {DomSanitizer} from '@angular/platform-browser';
import {SharedHttpService} from "../../services/SharedHttp.service";

@Component({
    selector: 'app-settings-company',
    templateUrl: './settings-company.component.html',
    styles: []
})
export class SettingsCompanyComponent implements OnInit, OnDestroy {
    public title = 'Dashboard';
    public outlets:Store[] = [];
    public products = [];
    public taxonomies = [];
    public warehouses:Store[] = [];
    public company = [];
    public currentUser:User;
    public isOnline = true;
    public loading = false;
    public path:any;
    public link:any;
    public companyData:any;
    form_group:FormGroup;
    FormErr = '';
    public FormState = '';
    public responseType = 'd-none';
    private subscription:Subscription;

    constructor(private storageService:StorageService, private transporter:TransporterService, private httpService:SharedHttpService, private sanitizer:DomSanitizer, private fb:FormBuilder) {
        this.form_group = this.fb.group({
            Name: ['', Validators.required],
            RefundPolicy: ['', Validators.required],
        });

        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'get-company-data') {
                this.companyData = parcel.data;
                this.form_group.setValue({
                    Name: this.companyData.Name,
                    RefundPolicy: this.companyData.RefundPolicy,
                });
                this.path = this.companyData.Logo;
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    sanitize(url:string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }


    async onFileChange(event) {
        this.loading = true;
        const page = this;
        if (event.target.files && event.target.files.length > 0) {
            const file = event.target.files[0];
            this.path = URL.createObjectURL(event.target.files[0]);
            this.storageService.addFile(file, null, (link, xpath) => {
                this.link = link;
                console.log('Link', link);
                this.loading = false;

                // todo: use image from here , (page.)
            });
        } else {
            this.loading = false;
            this.path = '';
            this.link = null;
        }
    }

    submit() {

        this.httpService.Patch('/companies/' + this.currentUser.Company, this.form_group.value).subscribe(data=> {
            console.log(data);
            this.FormState = '';
            this.responseType = 'success';
            this.FormErr = 'Done.';
            setTimeout(() => {
                this.responseType = 'd-none';
            }, 3000);
            this.transporter.transmit({type: 'refresh-loyalty-program'});
        }, err=>console.log(err));
    }


}
