import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsInventoryTagsComponent } from './settings-inventory-tags.component';

describe('SettingsInventoryTagsComponent', () => {
  let component: SettingsInventoryTagsComponent;
  let fixture: ComponentFixture<SettingsInventoryTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsInventoryTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsInventoryTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
