import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store, User} from '../../models/AllModels';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../../services/transporter.service';
import {StorageService} from "../../services/storage.service";
import {DomSanitizer} from '@angular/platform-browser';
import {SharedHttpService} from "../../services/SharedHttp.service";

@Component({
  selector: 'app-loyalty-program',
  templateUrl: './loyalty-program.component.html',
  styles: []
})
export class LoyaltyProgramComponent implements OnInit {
  public title = 'Dashboard';
    public responseType = 'd-none';
  public outlets:Store[] = [];
  public products = [];
  public taxonomies = [];
  public warehouses:Store[] = [];
  public company = [];
  public currentUser:User;
  public isOnline = true;
  public loading = false;
  public path:any;
  public link:any;
  public companyData:any;
  form_group:FormGroup;

    public FormState = '';
  FormErr = '';
  private subscription:Subscription;

  constructor(private storageService:StorageService, private transporter:TransporterService, private httpService:SharedHttpService, private sanitizer:DomSanitizer, private fb:FormBuilder) {
      this.subscription = this.transporter.receive().subscribe(parcel => {
          if (parcel.type === 'get-company-data') {
              this.companyData = parcel.data;
              this.form_group.setValue({
                  IsLoyaltyProgram: this.companyData.IsLoyaltyProgram,
                  LoyaltyPointWorth: this.companyData.LoyaltyPointWorth,
                  LoyaltyRewardRate: this.companyData.LoyaltyRewardRate,
                  LoyaltyPointsValidity: this.companyData.LoyaltyPointsValidity,
              });
          }
      });
  }

  ngOnInit() {
    console.log('link',this.link);
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.form_group = this.fb.group({
      IsLoyaltyProgram: [false, Validators.required],
      LoyaltyPointWorth: ['', Validators.required],
      LoyaltyRewardRate: ['', Validators.required],
      LoyaltyPointsValidity: ['', Validators.required],
    });

  }

  submit() {

    this.httpService.Patch('/companies/' + this.currentUser.Company, this.form_group.value).subscribe(data=> {
      console.log(data);
        this.FormState = '';
        this.responseType = 'success';
        this.FormErr = 'Done.';
        setTimeout(() => {
            this.responseType = 'd-none';
        }, 3000);
        this.transporter.transmit({type: 'refresh-loyalty-program'});
    }, err=>console.log(err));

  }


}
