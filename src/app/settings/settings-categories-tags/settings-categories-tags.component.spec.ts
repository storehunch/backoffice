import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsCategoriesTagsComponent } from './settings-categories-tags.component';

describe('SettingsCategoriesTagsComponent', () => {
  let component: SettingsCategoriesTagsComponent;
  let fixture: ComponentFixture<SettingsCategoriesTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsCategoriesTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsCategoriesTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
