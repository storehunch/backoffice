import {Component, OnInit} from '@angular/core';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SharedHttpService } from '../services/SharedHttp.service';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {
    SignUpForm: FormGroup;
    NewUser: String;
    FormErr: String = '';
    FormData: any;
    FormState: String = '';

    constructor (private fb: FormBuilder, private httpService: SharedHttpService) {}

    ngOnInit() {
        this.SignUpForm = this.fb.group({
            Name: ['', Validators.required],
            FirstName: ['', Validators.required],
            LastName: ['', Validators.required],
            MobileNumber: ['', Validators.required],
            Email: ['', Validators.email],
            Username: ['', Validators.required],
            Password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    private CreateUser() {
        this.httpService.Post('/users', this.SignUpForm.value)
            .subscribe(
                data => {
                    this.FormData = data; this.FormState = '';
                    this.NewUser = data.toString();
                },
                err  => {
                    this.FormState = '';
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                        }
                    }
                }
            );
    }

    submit() {
        if (this.SignUpForm.valid) {
            this.FormState = 'loading';
            this.CreateUser();
        } else {
            Object.keys(this.SignUpForm.controls).forEach(field => { // {1}
                const control = this.SignUpForm.get(field);            // {2}
                control.markAsDirty({ onlySelf: true });       // {3}
            });
        }
    }
}
