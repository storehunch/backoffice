import {Component, OnInit, OnDestroy} from '@angular/core';
import {User, Store} from '../models/AllModels';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {
    public title = 'Dashboard';
    public outlets: Store[] = [];
    public products = [];
    public taxonomies = [];
    public warehouses: Store[] = [];
    public company = [];
    public currentUser: User;
    public isOnline = true;
    private subscription: Subscription;

    constructor(private transporter: TransporterService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'outlets') {
                this.outlets = parcel.data;
            } else if (parcel.type === 'warehouses') {
                this.warehouses = parcel.data;
            }

            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'company') {
                this.company = parcel.data;
            }
            if (parcel.type === 'products') {
                this.products = parcel.data;
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
