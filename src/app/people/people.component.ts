import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router } from '@angular/router';

import { SharedHttpService } from '../services/SharedHttp.service';
import {User} from '../models/User';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styles: []
})
export class PeopleComponent implements OnInit {
    @Input() LocationId: string;
    public Users: User[] = [];
    public IsReady = false;
    public CurrentUser: User = JSON.parse(localStorage.getItem('user'));

    constructor (private fb: FormBuilder, private httpService: SharedHttpService, private router: Router) {}
    ngOnInit() {

        this.IsReady = false;
        this.GetUsers();
    }
    private GetUsers() {
        const preUrl = '/users?company=' + this.CurrentUser.Company + '&store=' + this.LocationId;

        this.httpService.Get(preUrl)
            .subscribe(
                res => {
                    this.IsReady = true;
                    if (res['status'] !== 204) {
                        this.Users = res['body']['data'];
                    }else {
                        this.Users = [];
                    }
                },
                err  => {
                    console.log('Unexpected error, Please contact support.');
                }
            );
    }

    getRowHeight(row) {
        return row.height;
    }
}
