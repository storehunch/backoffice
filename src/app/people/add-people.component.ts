import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SharedHttpService } from '../services/SharedHttp.service';
import {ActivatedRoute, Router} from '@angular/router';
import { User } from '../models/User';

@Component({
  selector: 'app-add-people',
  templateUrl: './add-people.component.html',
  styles: []
})
export class AddPeopleComponent implements OnInit {
    @Input() LocationId: string;
    PeopleForm: FormGroup;
    FormErr: String = '';
    FormState: String = '';
    AccessLevels = [];
    public IsEdit = false;
    public EditUser: string;
    public UserToEdit = [];
    CurrentUser: User = JSON.parse(localStorage.getItem('user'));
    constructor (private activatedRoute: ActivatedRoute, private fb: FormBuilder, private httpService: SharedHttpService, private router: Router) {}

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.IsEdit = !!params['edit'];
            this.EditUser = params['edit'];
        });
        this.AccessLevels = ['Admin', 'Staff'];
        this.PeopleForm = this.fb.group({
            FirstName: ['', Validators.required],
            LastName: ['', Validators.required],
            MobileNumber: ['', Validators.required],
            Email: ['', Validators.email],
            Username: ['', Validators.required],
            Company: [this.CurrentUser.Company],
            UserType: ['Client'],
            AccessLevel: ['Staff', Validators.required],
            Store: [this.LocationId],
            Password: ['', [Validators.required, Validators.minLength(6)]]
        });
        if (this.IsEdit) {
            this.GetUser();
        }
    }

    private CreateUser() {
        this.httpService.Post('/users', this.PeopleForm.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.router.navigate(['/locations/' + this.LocationId + '/users']);
                },
                err  => {
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }

                        }
                    }
                }
            );
    }
    private PatchUser() {
        this.httpService.Patch('/users/' + this.EditUser, this.PeopleForm.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.router.navigate(['/locations/' + this.LocationId + '/users']);
                },
                err  => {
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }

                        }
                    }
                }
            );
    }

    GetUser() {
        this.httpService.Get('/users/' + this.EditUser)
            .subscribe(
                res => {
                    this.UserToEdit = res['body'];
                    this.PeopleForm.setValue({
                        FirstName: this.UserToEdit['FirstName'],
                        LastName: this.UserToEdit['LastName'],
                        MobileNumber: this.UserToEdit['MobileNumber'],
                        Email: this.UserToEdit['Email'],
                        Username: this.UserToEdit['Username'],
                        Company: this.UserToEdit['Company'],
                        UserType: 'Client',
                        AccessLevel: this.UserToEdit['AccessLevel'],
                        Store: this.LocationId,
                        Password: ''
                    });
                },
                err  => {
                    if (err.error && err.status !== 404) {
                        console.log('Unexpected error');
                    }else {
                        this.UserToEdit = null;
                    }
                }
            );
    }

    submit() {
        if (this.PeopleForm.valid) {
            this.FormState = 'loading';
            if ( !this.IsEdit ) {
                this.CreateUser();
            }else {
                this.PatchUser();
            }
        } else {
            Object.keys(this.PeopleForm.controls).forEach(field => { // {1}
                const control = this.PeopleForm.get(field);            // {2}
                control.markAsDirty({ onlySelf: true });       // {3}
            });
        }
    }
}
