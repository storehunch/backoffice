import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DexieService} from '../services/dexie.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        DexieService,
    ],
    declarations: []
})
export class DexieModule {
    constructor(@Optional() @SkipSelf() parentModule: DexieModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}
