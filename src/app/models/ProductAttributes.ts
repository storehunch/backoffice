export class ProductAttribute {
    _id: string;
    Product: string;
    Store: string;
    Quantity: number;
}
