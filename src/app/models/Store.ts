export class Store {
    _id: string;
    Name: string;
    Phone: string;
    Email: string;
    StoreType: string; /*WareHouse,Outlet*/
    Searchstring: string;
    Street: string;
    City: string;
    State: string;
    Country: string;
    Postal: string;
    Lat: any;
    Long: any;
    Currency: string;
    CurrencySymbol: string;
    ReceiptPrinter: string;
    ReceiptPageSize: string;
    LabelPrinter: string;
    Company: string;
    Referrer: string;
}