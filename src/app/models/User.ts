export class User {
    _id: string;
    FirstName: string;
    LastName: string;
    Dp: string;
    Username: string;
    Email: string;
    Salary: number;
    Store: string;
    Company: string;
    MobileNumber: string;
    Password: string;
    Level: string;
    ReferralOnly: Boolean;
    Verified: Boolean;
    CreatedBy: string;
    Street: string;
    City: string;
    State: string;
    Country: string;
    PostalCode: string;
    AccessLevel: string;
    UserType: string; /*President,Customer,Supplier,Client*/
}
