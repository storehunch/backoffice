export class ShippingOut {
    IsReceived: boolean; /* Sent,Received */
    To: string;
    From: string;
    ShippedBy: string;
    ReceivedBy: string;
    Company: string;
    DateTime: Date;
    Quantity: number;
}
