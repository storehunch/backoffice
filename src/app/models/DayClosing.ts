export class DayClosing {
    _id: string;
    Date: Date;
    Store: string;
    Company: string;
    Cash: number;
    LocalId: string;
    Revenue: number;
    Profit: number;
    Loss: number;
    Sales: number;
    Returns: number;
    User: string;
}
