import { Component, OnInit } from '@angular/core';
import {SharedHttpService} from '../../services/SharedHttp.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/User';
import * as $ from 'jquery';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {DexieService} from '../../services/dexie.service';
import {TransporterService} from '../../services/transporter.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-account-new',
  templateUrl: './account-new.component.html',
  styles: []
})
export class AccountNewComponent implements OnInit {

  public isOnline = true;
  public company = [];
  public accountToEdit;
  public responseType = 'd-none';
  public dataForm: FormGroup;
  public FormErr: String = '';
  public ModalState = false;
  public editId = null;
  public FormState: String = '';
  public exitAfterSave = true;
  public currentUser: User;
  public locationId = '';
  public outlets = [];
  public warehouses = [];
  private subscription: Subscription;

  constructor(public dexieService: DexieService, private transporter: TransporterService, private activatedRoute: ActivatedRoute, private fb: FormBuilder, private httpService: SharedHttpService) {
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'connection') {
        this.isOnline = parcel.data;
      }
      if (parcel.type === 'new-account') {
        if (parcel.data === 'start') {
          $('body').removeClass('overflow-hidden');
          this.ToggleModal();
        }
      }
      if (parcel.type === 'edit-account') {
        if (parcel.data) {
          $('body').removeClass('overflow-hidden');
          this.editId = parcel.data;
          this.ToggleModal();
          this.GetAccount();
        }
      }
      if (parcel.type === 'outlets') {
        this.outlets = parcel.data;
      }
      if (parcel.type === 'warehouses') {
        this.warehouses = parcel.data;
      }
    });
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
    this.dexieGetStores();
    this.activatedRoute.params.subscribe(params => {
      this.locationId = params['id'];
    });
    this.dataForm = this.fb.group({
      Title: ['', Validators.required],
      Identifier: ['', Validators.required],
      AccountType: ['CashDrawer', Validators.required],
      Store: [''],
      AccountDescription: ['']
    });
  }

  dexieGetStores() {
    this.dexieService['Store'].where({
      StoreType: 'Warehouse',
      Company: this.currentUser.Company
    }).toArray().then(data => {
      this.warehouses = data;
    }).catch(err => {
      console.log(err);
      this.warehouses = [];
    });
    this.dexieService['Store'].where({StoreType: 'Outlet', Company: this.currentUser.Company}).toArray().then(data => {
      this.outlets = data;
    }).catch(err => {
      console.log(err);
      this.outlets = [];
    });
  }

  GetAccount() {
    this.httpService.Get('/accounts/' + this.editId)
      .subscribe(
        res => {
          this.accountToEdit = res['body'];
          this.dataForm.setValue({
            Title: this.accountToEdit.Title ? this.accountToEdit.Title : '',
            Identifier: this.accountToEdit.Identifier ? this.accountToEdit.Identifier : '',
            AccountType: this.accountToEdit.AccountType ? this.accountToEdit.AccountType : 'drawer',
            Store: this.accountToEdit.Store ? this.accountToEdit.Store : this.locationId,
            AccountDescription: this.accountToEdit.AccountDescription ? this.accountToEdit.AccountDescription : '',
          });
        },
        err => {
          if (err.error && err.status !== 404) {
            console.log('Unexpected error');
          } else {
            this.accountToEdit = null;
          }
        }
      );
  }


  resetForm() {
    this.responseType = 'd-none';
    this.FormErr = '';
    this.dataForm.reset({
      Title: '',
      Identifier: '',
      AccountType: 'drawer',
      AccountDescription: '',
      Store: ''
    });
    this.dataForm.get('Title').setValidators(Validators.required);
    this.dataForm.get('Identifier').setValidators(Validators.required);
    this.dataForm.get('AccountType').setValidators(Validators.required);
    this.dataForm.get('AccountDescription').setValidators(Validators.required);
    if (this.exitAfterSave) {
      this.ToggleModal();
    }
  }

  checkForNumber(e) {
    if (e.target.value.length) {
      const val = +e.target.value;
      if (!val) {
        e.target.value = 1;
      }
    }
  }

  ToggleModal() {
    $('body').toggleClass('overflow-hidden');
    return this.ModalState = !this.ModalState;
  }

  PostAccount() {
    this.httpService.Post('/accounts', this.dataForm.value)
      .subscribe(
        data => {
          console.log(data);
          this.FormState = '';
          this.responseType = 'success';
          this.FormErr = 'Account created successfully.';
          this.transporter.transmit({type: 'get-accounts', data: true});
          setTimeout(() => {
            this.responseType = 'd-none';
          }, 3000);
          this.resetForm();
        },
        err => {
          if (err.error) {
            const data = err.error.errors;
            this.FormState = '';
            this.responseType = 'error';
            for (const key of Object.keys(data)) {
              if (key !== 'Company') {
                if (data[key].kind === 'required') {
                  this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                } else {
                  this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                }
              }
            }
            setTimeout(() => {
              this.responseType = 'd-none';
            }, 4000);
            this.dataForm.reset(this.dataForm.value);
          }
        }
      );
  }

  PatchAccount() {
    this.httpService.Patch('/accounts/' + this.editId, this.dataForm.value)
      .subscribe(
        data => {
          console.log(data);
          this.FormState = '';
          this.responseType = 'success';
          this.FormErr = 'Account Updated successfully.';
          this.transporter.transmit({type: 'get-accounts', data: true});
          setTimeout(() => {
            this.responseType = 'd-none';
          }, 3000);
          this.resetForm();
        },
        err => {
          if (err.error) {
            const data = err.error.errors;
            this.FormState = '';
            this.responseType = 'error';
            for (const key of Object.keys(data)) {
              if (key !== 'Company') {
                if (data[key].kind === 'required') {
                  this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                } else {
                  if (key === 'CompanyBarcode') {
                    this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                  } else {
                    this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                  }
                }
              }
            }
            setTimeout(() => {
              this.responseType = 'd-none';
            }, 4000);
            this.dataForm.reset(this.dataForm.value);
          }
        }
      );
  }

  submit(exit) {
    this.exitAfterSave = exit;
    if (this.dataForm.valid) {
      if (!this.editId) {
        this.dataForm.value.User = this.currentUser._id;
        this.dataForm.value.Company = this.currentUser.Company;
        this.dataForm.value.Balance = 0;
        if (this.dataForm.value.Store === '') {
          delete this.dataForm.value.Store;
        }
        this.PostAccount();
      } else {
        this.PatchAccount();
      }

    } else {
      console.log(this.dataForm.value);
      console.log('not valid');
      Object.keys(this.dataForm.controls).forEach(field => {
        const control = this.dataForm.get(field);
        control.markAsDirty({onlySelf: true});
      });
    }
  }
}
