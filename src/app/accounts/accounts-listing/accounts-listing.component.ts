import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/User';
import {TransporterService} from '../../services/transporter.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {SharedHttpService} from '../../services/SharedHttp.service';

@Component({
  selector: 'app-accounts-listing',
  templateUrl: './accounts-listing.component.html',
  styles: []
})
export class AccountsListingComponent implements OnInit {

  public accounts = [];
  public currentUser: User;
  public isOnline = true;
  private subscription: Subscription;
  public locationId: string;
  public company = [];
  loadingIndicator = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private transporter: TransporterService, private activatedRoute: ActivatedRoute, private httpService: SharedHttpService) {
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'connection') {
        this.isOnline = parcel.data;
      }
      if (parcel.type === 'company') {
        this.company = parcel.data;
      }
    });
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'get-accounts') {
        this.getAccounts();
      }
    });
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.locationId = this.currentUser.Store ? this.currentUser.Store : '';
    this.getAccounts();
  }


  getAccounts() {
    let accountsUrl = 'company=' + this.currentUser.Company;
    accountsUrl = this.currentUser.Store ? accountsUrl + '&store=' + this.locationId : accountsUrl;
    this.httpService.Get('/accounts?' + accountsUrl )
      .subscribe(
        res => {
          if (res['status'] !== 204) {
           this.accounts = res['body']['data'];
            console.log('acounts', this.accounts);
          }else {
            this.accounts = [];
          }
          this.loadingIndicator = false;
        },
        err => {
          if (this.isOnline) {
            console.log('Unexpected error');
          }
          this.loadingIndicator = false;
        }
      );

  }


  transport(packet: any): void {
    this.transporter.transmit(packet);
  }

  triggerTransaction() {
    $('body').removeClass('overflow-hidden');
    this.transport({type: 'new-transaction', data: 'start'});
  }

  triggerDeposit(id) {
    $('body').removeClass('overflow-hidden');
    this.transport({type: 'new-transaction', data: id});
  }


  editAccount(pid) {
    $('body').removeClass('overflow-hidden');
    this.transport({type: 'edit-account', data: pid});
  }

  triggerCreateAccount() {
    $('body').removeClass('overflow-hidden');
    this.transport({type: 'new-account', data: 'start'});
  }

}
