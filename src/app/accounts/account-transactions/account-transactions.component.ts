import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../../models/User';
import {ActivatedRoute} from '@angular/router';
import {TransporterService} from '../../services/transporter.service';
import {SharedHttpService} from '../../services/SharedHttp.service';
import {PrintService} from "../../services/print.service";

@Component({
    selector: 'app-account-transactions',
    templateUrl: './account-transactions.component.html',
    styles: []
})
export class AccountTransactionsComponent implements OnInit {
    id: any;
    public currentAccount = [];
    public transactions = [];
    public transactionsTo = [];
    public transactionsFrom = [];
    loadingIndicator = true;
    public isOnline = true;
    public company = [];
    public currentUser: User;
    private subscription: Subscription;

    constructor(private printService: PrintService, private route: ActivatedRoute, private transporter: TransporterService, private httpService: SharedHttpService) {
        this.id = route.snapshot.params['aid'];
        this.getAccount(this.id);
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'company') {
                this.company = parcel.data;
            }
            if (parcel.type === 'get-transactions') {
                this.getTransactionsTo();
                this.getAccount(this.id);
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.getTransactionsTo();
    }

    getTransactionsTo() {
        const transactionsUrl = '/transactions?to=' + this.id + '&company=' + this.currentUser.Company;
        this.httpService.Get(transactionsUrl)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.transactionsTo = res['body']['data'];
                        console.log('transactions to', this.transactionsTo);
                    } else {
                        this.transactionsTo = [];
                        console.log('transactions to', this.transactionsTo);
                    }
                    this.getTransactionsFrom();
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error');
                    }
                    console.log(err);
                    this.loadingIndicator = false;
                }
            );

    }

    getTransactionsFrom() {
        const transactionsUrl = '/transactions?from=' + this.id + '&company=' + this.currentUser.Company;
        this.httpService.Get(transactionsUrl)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.transactionsFrom = res['body']['data'];
                        console.log('transactions from', this.transactionsFrom);
                    } else {
                        this.transactionsFrom = [];
                    }
                    this.transactions = this.transactionsTo.concat(this.transactionsFrom);
                    console.log('transactions ', this.transactions);
                    this.transactions.sort(function (a, b) {
                        a = new Date(a.Date);
                        b = new Date(b.Date);
                        return a > b ? -1 : a < b ? 1 : 0;
                    });
                    this.loadingIndicator = false;
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error');
                    }
                    console.log(err);
                    this.loadingIndicator = false;
                }
            );
    }

    printReport() {
        let temp = this.clone(this.transactions);
        let page = this;
        const mappedRows = temp.map(function (item) {

            item['Transaction'] = '';
            if (item['From'] && item['To']) {
                item['Transaction'] = item['From'].Title + ' -> ' + item['To'].Title;
            } else {
                item['Transaction'] = '-';

            }
            if (item['From'] && item['Supplier']) {
                item['Transaction'] = item['From'].Title + ' -> ' + item['Supplier'].Name;

            }
            item['Balance'] = item['FromBalance'];
            const mDate = item['createdAt'].split('T')[0].split('-')[2] + '-' + item['createdAt'].split('T')[0].split('-')[1] + '-' + item['createdAt'].split('T')[0].split('-')[0];
            // const mTime = item['createdAt'].split('T')[1].split(':')[2] + '-' + item['createdAt'].split('T')[1].split(':')[1] + '-' + item['createdAt'].split('T')[1].split(':')[0];

            item['Date'] = mDate;
            item['Debit'] = item.To !== undefined && item.To._id === page.id ? item['Amount'] : ' - ';
            item['Credit'] = item.From && item.From._id === page.id ? item['Amount'] : ' - ';
            item['Date'] = mDate;

            delete item['_id'];
            delete item['Supplier'];
            delete item['updatedAt'];
            delete item['createdAt'];
            delete item['Company'];
            delete item['__v'];
            delete item['User'];
            delete item['From'];
            delete item['To'];
            delete item['ToBalance'];
            delete item['FromBalance'];
            return item;
        });
        this.printService.printReport([mappedRows], 'Category Wise Sales Report', '', '', ['', 'Transactions']);
    }

    formatTime(time, prefix = '') {
        return typeof time === 'object' ? prefix + time.toLocaleDateString() : '';
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }

    triggerTransaction() {
        $('body').removeClass('overflow-hidden');
        this.transport({type: 'new-transaction', data: 'start'});
    }

    private getAccount(id: string) {
        this.httpService.Get('/accounts/' + id)
            .subscribe(
                res => {
                    this.currentAccount = res['body'];
                },
                err => {
                    this.currentAccount = [];
                }
            );
    }

}
