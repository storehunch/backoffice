import { Component, OnInit } from '@angular/core';
import {TransporterService} from '../../services/transporter.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DexieService} from '../../services/dexie.service';
import {Subscription} from 'rxjs/Subscription';
import {SharedHttpService} from '../../services/SharedHttp.service';
import {User} from '../../models/User';
import * as $ from 'jquery';
import {ActivatedRoute} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styles: []
})
export class TransactionComponent implements OnInit {

  public isOnline = true;
  public sameAccountErr = false;
  public responseType = 'd-none';
  public dataForm: FormGroup;
  public FormErr: String = '';
  public ModalState = false;
  public FormState: String = '';
  public currentUser: User;
  public transactionFrom = '';
  public suppliers = [];
  public accounts = [];
  public banks = [];
  public creditCards = [];
  public cashDrawers = [];
  public cashInHand = [];
  public others = [];
  public person = [];
  public users = [];
  public isDeposit = false;
  public outlets = [];
  public warehouses = [];
  private subscription: Subscription;
  public depositTo = '';

  constructor(public dexieService: DexieService, private transporter: TransporterService, private activatedRoute: ActivatedRoute, private fb: FormBuilder, private httpService: SharedHttpService) {
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'connection') {
        this.isOnline = parcel.data;
      }
      if (parcel.type === 'new-transaction') {
        $('body').removeClass('overflow-hidden');
        this.ToggleModal();
        if (parcel.data !== 'start' ) {
          this.isDeposit = true;
          this.depositTo = parcel.data;
          if (this.dataForm) {
            this.dataForm.controls['To'].setValue(this.depositTo);
            this.dataForm.get('From').clearValidators();
          }
        }else {
          this.dataForm.get('From').setValidators(Validators.required);
          this.isDeposit = false;
        }
      }
      if (parcel.type === 'outlets') {
        this.outlets = parcel.data;
      }
      if (parcel.type === 'warehouses') {
        this.warehouses = parcel.data;
      }
    });
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.dexieGetStores();
    this.GetUsers();
    this.GetAccounts();
    this.dataForm = this.fb.group({
      From: ['', Validators.required],
      To: [this.depositTo, Validators.required],
      Amount: [1, [Validators.required, Validators.min(1)]],
      Purpose: ['', Validators.required],
      User: [this.currentUser['_id'], Validators.required],
      Company: [this.currentUser['Company'], Validators.required],
      Ref: ['']
    });
    if (!this.isDeposit) {
      this.dataForm.get('From').setValidators(Validators.required);
    }
  }

  dexieGetStores() {
    this.dexieService['Store'].where({
      StoreType: 'Warehouse',
      Company: this.currentUser.Company
    }).toArray().then(data => {
      this.warehouses = data;
    }).catch(err => {
      console.log(err);
      this.warehouses = [];
    });
    this.dexieService['Store'].where({StoreType: 'Outlet', Company: this.currentUser.Company}).toArray().then(data => {
      this.outlets = data;
    }).catch(err => {
      console.log(err);
      this.outlets = [];
    });
  }

  GetUsers() {
    this.httpService.Get('/users?company=' + this.currentUser.Company)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.users = res['body']['data'];
          }else {
            this.users = [];
          }
        },
        err => {
          this.users = [];
        }
      );
  }

  GetAccounts() {
    this.httpService.Get('/accounts?company=' + this.currentUser.Company)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.accounts = res['body']['data'];
            this.groupAccounts();
          }else {
            this.accounts = [];
          }
        },
        err => {
          this.users = [];
        }
      );
  }

  groupAccounts() {
    this.banks = this.accounts.filter( account => {
      if (account.AccountType === 'BankAccount') {
        return account;
      }
    });
    this.cashInHand = this.accounts.filter( account => {
      if (account.AccountType === 'CashInHand') {
        return account;
      }
    });
    this.cashDrawers = this.accounts.filter( account => {
      if (account.AccountType === 'CashDrawer' || account.AccountType === 'drawer') {
        return account;
      }
    });
    this.creditCards = this.accounts.filter( account => {
      if (account.AccountType === 'CreditCard') {
        return account;
      }
    });
    this.others = this.accounts.filter( account => {
      if (account.AccountType === 'Other') {
        return account;
      }
    });
  }

  resetForm() {
    this.responseType = 'd-none';
    this.FormErr = '';
    this.dataForm.reset({
      From: '',
      To: '',
      Amount: 1,
      Purpose: '',
      Ref: '',
      User: this.currentUser['_id'],
      Company: this.currentUser['Company']
    });
    this.dataForm.get('To').setValidators(Validators.required);
    this.dataForm.get('Amount').setValidators([Validators.required, Validators.min(1)]);
    this.dataForm.get('Purpose').setValidators(Validators.required);
    this.dataForm.get('User').setValidators(Validators.required);
    this.dataForm.get('Company').setValidators(Validators.required);
    this.ToggleModal();
  }

  checkForNumber(e) {
    if (e.target.value.length) {
      const val = +e.target.value;
      if (!val) {
        e.target.value = 1;
      }
    }
  }

  ToggleModal() {
    $('body').toggleClass('overflow-hidden');
    return this.ModalState = !this.ModalState;
  }

  PostTransaction() {
    this.httpService.Post('/transactions', this.dataForm.value)
      .subscribe(
        data => {
          console.log(data);
          this.FormState = '';
          this.responseType = 'success';
          this.FormErr = '<div class="alert alert-success alert-dismissible fade show mt-10" role="alert">Transaction has been made successfully.</div>';
          this.transporter.transmit({type: 'get-transactions', data: true});
          this.transporter.transmit({type: 'get-accounts', data: true});
          setTimeout(() => {
            this.responseType = 'd-none';
          }, 3000);
          this.resetForm();
        },
        err => {
          if (err.error) {
            const data = err.error.errors;
            this.FormState = '';
            this.responseType = 'error';
            for (const key of Object.keys(data)) {
              if (key !== 'Company') {
                if (data[key].kind === 'required') {
                  this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                } else {
                  this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                }
              }
            }
            setTimeout(() => {
              this.responseType = 'd-none';
            }, 4000);
            this.dataForm.reset(this.dataForm.value);
          }
        }
      );
  }


  selectFrom(e) {
    this.transactionFrom = e.target.value;
  }
  selectTo(e) {
    if (this.transactionFrom ===  e.target.value) {
      this.sameAccountErr = true;
      setTimeout(() => {
        e.target.value = '';
      }, 2000);
      setTimeout(() => {
        this.sameAccountErr = false;
      }, 5000);
    }
  }

  submit() {
    console.log(this.dataForm.value);
    if (this.dataForm.valid) {
      if (this.dataForm.value.From === '') {
        delete this.dataForm.value.From;
      }
      this.dataForm.value.Date = moment().utcOffset(new Date().getTimezoneOffset());
      this.PostTransaction();
    } else {
      console.log(this.dataForm.value);
      console.log('not valid');
      Object.keys(this.dataForm.controls).forEach(field => {
        const control = this.dataForm.get(field);
        control.markAsDirty({onlySelf: true});
      });
    }
  }

}
