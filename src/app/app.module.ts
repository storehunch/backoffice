import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {NgxBarcodeModule} from 'ngx-barcode';

import {AuthGuardGuard} from './auth.guard';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {NavigationComponent} from './shared/navigation.component';
import {SharedHttpService} from './services/SharedHttp.service';
import {Page404Component} from './shared/page404.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PeopleComponent} from './people/people.component';
import {AddPeopleComponent} from './people/add-people.component';
import {AddLocationComponent} from './location/add-location.component';
import {LocationDetailUsersComponent} from './location/location-detail-users.component';
import {LocationDetailSettingComponent} from './location/location-detail-setting.component';
import {NewUserComponent} from './location/new-user.component';
import {TaxonomyComponent} from './taxonomy/taxonomy.component';
import {SupplierComponent} from './supplier/supplier.component';
import {DayComponent} from './day/day.component';
import {ReportsComponent} from './reports/reports.component';
import {SidebarComponent} from './shared/sidebar.component';
import {LocationOverviewComponent} from './location/location-overview.component';
import {ReceiptsComponent} from './receipts/receipts.component';
import {DexieModule} from './dexie/dexie.module';
import {DexieCrudService} from './services/dexie-crud.service';
import {HeartbeatService} from './services/heartbeat.service';
import {TransporterService} from './services/transporter.service';

import {SocketIoConfig, SocketIoModule} from 'ng-socket-io';
import {NetworkStatusComponent} from './shared/network-status.component';
import {ProductsComponent} from './products/products.component';
import {NewProductComponent} from './products/new-product.component';
import {ExcelImportComponent} from './excel-import/excel-import.component';
import {ExpenseComponent} from './expense/expense.component';
import {IncomingShipmentComponent} from './activity/incoming-shipment.component';
import {ProductsAddedComponent} from './activity/products-added.component';
import {ReceiptListingComponent} from './receipts/receipt-listing.component';
import {ReceiptDetailComponent} from './receipts/receipt-detail.component';
import {SupplierListingComponent} from './supplier/supplier-listing.component';
import {EscapeHtmlPipe} from './pipes/keep-html.pipe';
import {DatePipe} from '@angular/common';
import {SupplierPaybackComponent} from './supplier/supplier-payback/supplier-payback.component';
import {DayClosingComponent} from './day/day-closing/day-closing.component';
import {DayOpeningComponent} from './day/day-opening/day-opening.component';
import {DatewiseSalesJournalComponent} from './reports/datewise-sales-journal.component';
import {ProfitLossStatementComponent} from './reports/profit-loss-statement.component';
import {CumilativeStockSalesReportComponent} from './reports/cumilative-stock-sales-report.component';
import {CategoryWiseSalesReportComponent} from './reports/category-wise-sales-report.component';
import {ProductHistoryReportComponent} from './reports/product-history-report.component';
import {AccountNewComponent} from './accounts/account-new/account-new.component';
import {AccountsListingComponent} from './accounts/accounts-listing/accounts-listing.component';
import {AccountTransactionsComponent} from './accounts/account-transactions/account-transactions.component';
import {TransactionComponent} from './accounts/transaction/transaction.component';
import {SettingsComponent} from './settings/settings.component';
import {SettingsCompanyComponent} from "./settings/settings-company/settings-company.component";
import {SettingsExpenseCategoriesComponent} from "./settings/settings-expense-categories/settings-expense-categories.component";
import {SettingsInventoryTagsComponent} from "./settings/settings-inventory-tags/settings-inventory-tags.component";
import {SettingsInventoryCategoriesComponent} from "./settings/settings-inventory-categories/settings-inventory-categories.component";
import {SettingsCategoriesTagsComponent} from './settings/settings-categories-tags/settings-categories-tags.component';
import {ProductAuditComponent} from './products/product-audit/product-audit.component';
import {CustomersComponent} from './customers/customers.component';
import {NewCustomerComponent} from './customers/new-customer/new-customer.component';
import {StorageService} from "./services/storage.service";
import {LoyaltyProgramComponent} from './settings/loyalty-program/loyalty-program.component';
import {SettingsSchematicsComponent} from './settings/settings-schematics/settings-schematics.component';
import {AnalyticsComponent} from './analytics/analytics.component';
import {PrintService} from "./services/print.service";


const socket_config: SocketIoConfig = {url: 'https://api.storehunch.com', options: {autoConnect: true}};

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        SignupComponent,
        NavigationComponent,
        Page404Component,
        DashboardComponent,
        PeopleComponent,
        AddPeopleComponent,
        AddLocationComponent,
        LocationDetailUsersComponent,
        LocationDetailSettingComponent,
        NewUserComponent,
        TaxonomyComponent,
        SupplierComponent,
        DayComponent,
        ReportsComponent,
        SidebarComponent,
        LocationOverviewComponent,
        ReceiptsComponent,
        NetworkStatusComponent,
        ProductsComponent,
        NewProductComponent,
        ExcelImportComponent,
        ExpenseComponent,
        IncomingShipmentComponent,
        ProductsAddedComponent,
        ReceiptListingComponent,
        ReceiptDetailComponent,
        SupplierListingComponent,
        EscapeHtmlPipe,
        SupplierPaybackComponent,
        DayClosingComponent,
        DayOpeningComponent,
        DatewiseSalesJournalComponent,
        ProfitLossStatementComponent,
        CumilativeStockSalesReportComponent,
        CategoryWiseSalesReportComponent,
        ProductHistoryReportComponent,
        AccountNewComponent,
        AccountsListingComponent,
        AccountTransactionsComponent,
        TransactionComponent,
        SettingsComponent,
        SettingsCompanyComponent,
        SettingsExpenseCategoriesComponent,
        SettingsInventoryCategoriesComponent,
        SettingsInventoryTagsComponent,
        SettingsCategoriesTagsComponent,
        ProductAuditComponent,
        CustomersComponent,
        NewCustomerComponent,
        LoyaltyProgramComponent,
        SettingsSchematicsComponent,
        AnalyticsComponent
    ],
    imports: [
        BrowserModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
        SocketIoModule.forRoot(socket_config),
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        AppRoutingModule,
        NgxDatatableModule,
        NgxBarcodeModule,
        DexieModule
    ],
    providers: [AuthGuardGuard, HeartbeatService, PrintService, SharedHttpService, DexieCrudService, TransporterService, DatePipe, StorageService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
