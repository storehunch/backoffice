import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';

import * as $ from 'jquery';

import {SharedHttpService} from '../services/SharedHttp.service';
import {Store, User} from '../models/AllModels';
import {DexieService} from '../services/dexie.service';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit, OnDestroy {
    @Input() title: string;
    public currentUser: User;
    public products = [];
    public outlets: Store[] = [];
    public warehouses: Store[] = [];
    public company = [];
    public myStore;
    public locationId: any;
    public isOnline = true;
    private subscription: Subscription;

    constructor(private transporter: TransporterService, private dexieService: DexieService, private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title, private httpService: SharedHttpService) {
        this.activatedRoute.paramMap.subscribe(
            params => {
                if (params.get('id')) {
                    this.currentUser = JSON.parse(localStorage.getItem('user'));
                    this.locationId = this.currentUser.Store ? this.currentUser.Store : params.get('id');
                    this.getMyStore(this.locationId);
                }
            }
        );
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }

            if (parcel.type === 'get-products') {


                this.getUpdatedProducts();

            }

            if (parcel.type === 'get-products-after-audit') {
                this.getUpdatedProducts_2();
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.titleService.setTitle(this.title);
        this.dexieGetStores();
        this.dexieGetCompany();
        this.getUpdatedCompany();
        this.dexieGetProducts();
        this.getUpdatedProducts();
        this.getUpdated();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    toggleOffCanvasMenu() {
        $('body').toggleClass('overflow-hidden');
        $('.off-canvas').toggleClass('active');
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    triggerImport() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'import', data: 'start'});
    }

    dexieGetStores() {
        this.dexieService['Store'].where({
            StoreType: 'Warehouse',
            Company: this.currentUser.Company
        }).toArray().then(data => {
            this.warehouses = data;
            this.transport({type: 'warehouses', data: this.warehouses});
        }).catch(err => {
            this.warehouses = [];
            this.transport({type: 'warehouses', data: this.warehouses});
        });
        this.dexieService['Store'].where({
            StoreType: 'Outlet',
            Company: this.currentUser.Company
        }).toArray().then(data => {
            this.outlets = data;
            this.transport({type: 'outlets', data: this.outlets});
        }).catch(err => {
            this.outlets = [];
            this.transport({type: 'outlets', data: this.outlets});
        });
    }


    getUpdated() {
        let lastUpdated;
        if (this.isOnline) {
            this.dexieService['Flag'].where({Name: 'Store'}).toArray().then(data => {
                lastUpdated = data[0];
                this.GetStores(lastUpdated['Value']);
            }).catch(err => {
                this.GetStores(false);
            });
        }
    }

    dexieGetCompany() {
        this.dexieService['Company'].where({_id: this.currentUser.Company}).toArray().then(data => {
            this.company = data[0];
            this.transport({type: 'company', data: this.company});
        }).catch(err => {
            console.log(err);
        });
    }

    getUpdatedCompany() {
        let lastUpdated;
        if (this.isOnline) {
            this.dexieService['Flag'].where({Name: 'Company'}).toArray().then(data => {
                lastUpdated = data[0];
                this.GetCompany();
            }).catch(err => {
                this.GetCompany();
            });
        }
    }

    GetStores(updatedAt: any) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }
        this.httpService.Get('/stores?company=' + this.currentUser.Company + queryPart)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.dexieService['Store'].bulkPut(res['body']['data']).then(data => {
                            this.dexieService['Flag'].put({
                                Name: 'Store',
                                Value: new Date().toJSON()
                            }).catch(flagErr => {
                                console.log(flagErr);
                            });
                            this.dexieGetStores();
                        }).catch(syncErr => {
                            console.log(syncErr);
                        });
                    } else {
                        console.log('Offline stores are up-to-date');
                    }
                },
                err => {
                    if (this.isOnline) {
                        console.log(err.status);
                    }
                }
            );
    }

    GetCompany() {
        this.httpService.Get('/companies/' + this.currentUser.Company)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.dexieService['Company'].put(res['body']).then(data => {
                            this.dexieService['Flag'].put({
                                Name: 'Company',
                                Value: new Date().toJSON()
                            }).catch(flagErr => {
                                console.log(flagErr);
                            });
                            this.dexieGetCompany();
                        }).catch(syncErr => {
                            console.log(syncErr);
                        });
                    }
                },
                err => {
                    if (this.isOnline) {
                        console.log(err.status);
                    }
                }
            );
    }

    getMyStore(storeId: string) {
        this.dexieService['Store'].where('_id').equals(storeId).toArray().then(data => {
            this.myStore = data[0];
        }).catch(dexieErr => {
            this.myStore = {};
        });

    }

    dexieGetProducts() {
        this.dexieService['Product'].where({Company: this.currentUser.Company}).toArray()
            .then(data => {
                this.products = data;
                if (this.locationId) {
                    this.getUpdatedAttributes();
                }
                this.transport({type: 'products', data: this.products});
            }).catch(err => {
            console.log(err);
            this.products = [];
        });
    }


    getUpdatedProducts() {
        let lastUpdated;
        this.dexieService['Flag'].where({Name: 'Product'}).toArray().then(data => {
            lastUpdated = data[0];
            this.getProducts(lastUpdated['Value']);
        }).catch(err => {
            this.getProducts(false);
        });
    }

    getUpdatedProducts_2() {

        this.dexieService['Flag'].where({Name: 'Product'}).toArray().then(data => {
            this.getProducts(false);
        }).catch(err => {
            this.getProducts(false);
        });
    }

    getProducts(updatedAt: any) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }

        console.log('get products function called')
        console.log(queryPart)

        this.httpService.Get('/products?company=' + this.currentUser.Company + queryPart)
            .subscribe(
                res => {
                    console.log('products get', res);
                    if (res['status'] !== 204) {

                        this.dexieService['Flag'].put({Name: 'Product', Value: new Date().toJSON()}).catch(flagErr => {
                            console.log(flagErr);
                        });
                        this.dexieService['Product'].bulkPut(res['body']['data']).then(data => {
                            this.dexieGetProducts();
                        }).catch(dexieErr => {
                            console.log(dexieErr);
                        });
                    }
                },
                err => {
                    console.log(err)
                    if (this.isOnline) {
                        console.log('Unexpected error');
                    }
                }
            );
    }

    getUpdatedAttributes() {
        this.getAttributes(false);
    }

    appendProductAttributes() {
        if (this.locationId) {
            for (let p = 0; p < this.products.length; p++) {
                this.dexieService['Attribute'].where({
                    Product: this.products[p]._id,
                    Store: this.locationId
                }).toArray().then(data => {
                    if (data.length) {
                        this.products[p].Attribute = data[0];
                    } else {
                        this.products[p].Attribute = {Stock: 0, Cost: 0};
                    }
                }).catch(err => {
                    console.log('error in attribute');
                    this.products[p].Attribute = {Stock: 0, Cost: 0};
                });
            }
            this.transport({type: 'products', data: this.products});
        }
    }

    getAttributes(updatedAt: any) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }
        if (this.locationId) {
            this.httpService.Get('/productattributes?store=' + this.locationId + queryPart)
                .subscribe(
                    res => {
                        if (res['status'] !== 204) {
                            this.dexieService['Flag'].put({
                                Name: 'Attribute',
                                Store: this.locationId,
                                Value: new Date().toJSON()
                            }).catch(flagErr => {
                                console.log(flagErr);
                            });
                            this.dexieService['Attribute'].bulkPut(res['body']['data']).then(data => {
                                this.appendProductAttributes();
                            }).catch(dexieErr => {
                                console.log(dexieErr);
                            });
                        } else {
                            this.appendProductAttributes();
                        }
                    },
                    err => {
                        if (this.isOnline) {
                            console.log('Unexpected error');
                        }
                    }
                );
        }
    }

    triggerExpense() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'expense', data: 'start'});
    }

    triggerPaybackToSupplier() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'supplier-payback', data: 'start'});
    }
    triggerAddProduct() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'new-product', data: 'start'});
    }

    triggerTransaction() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'new-transaction', data: 'start'});
    }

    triggerCreateAccount() {
        this.toggleOffCanvasMenu();
        this.transport({type: 'new-account', data: 'start'});
    }

    logout() {
        localStorage.clear();
        this.dexieService['Flag'].clear().catch(e => {
            console.error(e);
        });
        this.dexieService['Company'].clear().catch(e => {
            console.error(e);
        });
        this.dexieService['Store'].clear().catch(e => {
            console.error(e);
        });
        this.router.navigate(['/login']);
    }


}
