import {Component, OnInit} from '@angular/core';
import {HeartbeatService} from '../services/heartbeat.service';
import {TransporterService} from '../services/transporter.service';

@Component({
    selector: 'app-network-status',
    templateUrl: './network-status.component.html',
    styles: []
})
export class NetworkStatusComponent implements OnInit {

    public isOnline: boolean;

    constructor(private transporter: TransporterService, private hb: HeartbeatService) {
    }
    ngOnInit() {
        this.isOnline = true;
        setInterval(() => {
            const connectionStatus = this.hb.heartBeat() ? this.hb.heartBeat() : false;
            this.isOnline = connectionStatus['connected'];
            this.transporter.transmit({type: 'connection', data: this.isOnline});
        }, 10000);
    }

}

