import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, RouterLinkActive} from '@angular/router';
import {User} from '../models/User';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styles: []
})
export class SidebarComponent implements OnInit {

    @Input() title: string;
    public currentUser: User;
    public LocationId: any;

    constructor( private activatedRoute: ActivatedRoute) {
    }
    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.LocationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
    }

}
