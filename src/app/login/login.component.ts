import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SharedHttpService } from '../services/SharedHttp.service';
import { DexieCrudService } from '../services/dexie-crud.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    LoginForm: FormGroup;
    LoginErr: Boolean = false;
    AuthError: Boolean = false;
    LoginData: any;
    public token: string;
    LoginState: String = '';

    constructor (private fb: FormBuilder, private dexieService: DexieCrudService, private httpService: SharedHttpService, private router: Router) {}

    ngOnInit() {
        if (localStorage.getItem('auth_token')) {
            this.router.navigate(['/']);
        }
        this.LoginForm = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    private GetUser (id: string) {
        this.httpService.Get('/users/' + id)
            .subscribe(
                res => {
                    localStorage.setItem('user', JSON.stringify(res['body']));
                    this.dexieService.put('User', res['body']).then(dRes => {
                        this.LoginState = '';
                        location.reload();
                    }).catch(dErr => {
                        console.log(dErr);
                    });
                },
                err  => {this.LoginErr = true; this.LoginState = ''; }
            );
    }
    submit() {
        this.LoginState = 'loading';
        this.AuthError = false;
        this.httpService.Authenticate(this.LoginForm.value)
            .subscribe(
                data => {
                    this.LoginData = data;
                    this.token = data['token'];
                    localStorage.setItem('auth_token', this.token);
                    this.GetUser(data['user']);
                },
                err  => { this.LoginErr = true; this.LoginState = ''; }
            );
    }

}
