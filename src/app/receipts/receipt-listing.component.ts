import {DexieService} from './../services/dexie.service';
import {SharedHttpService} from './../services/SharedHttp.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../models/User';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-receipt-listing',
    templateUrl: './receipt-listing.component.html',
    styles: []
})
export class ReceiptListingComponent implements OnInit {

    rows = [];
    public receipts = [];
    public currentUser: User;
    public isOnline = true;
    public locationId: string;
    filter = 'all'
    suppliers = [];
    loadingIndicator = true;
    public searchText = '';
    branches = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private subscription: Subscription;

    constructor(private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService, private dexieService: DexieService) {
    }

    ngOnInit() {
        const page = this;
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['type']) {
                this.changeFilter({target: {value: params['type'].replace(/"/g, '\'')}});

                // page.filterFunction(params['type'].replace(/"/g, '\''));
            }
        });

        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'outlets') {
                parcel.data.forEach(d => this.branches.push(d));
            } else if (parcel.type === 'warehouses') {
                parcel.data.forEach(d => this.branches.push(d));
            }
            if (parcel.type === 'receipt-listing') {


                this.changeFilter({target: {value: 'all'}});

            }

        });

        this.GetReceipts();
        this.dexieGetSuppliers();
    }


    getRowHeight(row) {
        return row.height;
    }

    dexieGetSuppliers() {
        this.dexieService['Supplier'].where({Company: this.currentUser.Company}).toArray()
            .then(data => {
                this.suppliers = data;
                for (let index = 0; index < this.suppliers.length; index++) {
                    const supplier = this.suppliers[index];

                }
                this.loadingIndicator = false;
            }).catch(err => {
            this.suppliers = [];
        });
    }

    updateFilter(val) {
        // this.rows = this.receipts;
        this.rows = this.rows.filter(function (receipt) {
            if (receipt['LocalId'] && val.length) {
                return receipt['LocalId'].indexOf(val) !== -1 || !val;
            } else {
                return receipt;
            }
        });
        this.table.offset = 0;
    }

    showReceiptDetail(receipt) {
        // ReferenceReceipt
        if (receipt['ReferenceReceipt']) {
            this.httpService.Get('/receipts/' + receipt['ReferenceReceipt'] + '?company=' + this.currentUser.Company + '&store=' + this.locationId)
                .subscribe(
                    res => {
                        console.log(res);

                        const store = res['body']['Store'].Name;
                        this.transporter.transmit({
                            type: 'receipt-detail', data: receipt, StoreName: store
                        });
                    },
                    err => {
                        console.log('err');
                    }
                );
        } else {
            this.transporter.transmit({
                type: 'receipt-detail', data: receipt
            });
        }


    }

    selectSupplier(event) {
        const id = event.target.value;
        this.rows = this.receipts.filter(function (d) {
            if (d['Supplier'] === id) {
                return true;
            }
        });
    }

    changeFilter(event) {
        const val = this.filter = event.target.value;
        // filter our data
        console.log(val);

        if (val === 'all') {
            this.rows = this.receipts;
        } else {
            this.rows = this.receipts.filter(function (d) {
                if (d.TransactionType) {
                    return d.TransactionType === val;
                }
                return false;
            });
        }


        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    GetReceipts() {
        this.httpService.Get('/receipts?company=' + this.currentUser.Company + '&store=' + this.locationId + '&sort=-Date')
            .subscribe(
                res => {
                    this.receipts = this.rows = res['body']['data'];
                    console.log(this.rows);
                    this.loadingIndicator = false;
                },
                err => {
                    console.log('err');
                }
            );

    }

}
