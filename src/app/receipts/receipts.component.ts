import {User} from './../models/User';
import {SharedHttpService} from './../services/SharedHttp.service';
import {Component, OnInit, HostListener} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {DexieService} from '../services/dexie.service';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Store} from '../models/Store';
import * as $ from 'jquery';
import {v1} from 'uuid';

const JsBarcode = require('jsbarcode');
import * as moment from 'moment';

function discountCheck(control: AbstractControl): ValidationErrors | null {
  const sub_total = +$('#subTotal').val();
  const discount_type = $('#discountType').val() ? $('#discountType').val() : 'percentage';
  if (discount_type === 'percentage') {
    return ((control.value === null) || control.value >= 0 && control.value < 100) ? null : {error: true};
  }else {
    return ((control.value === null) || control.value >= 0 && control.value < sub_total) ? null : {error: true};
  }
}

function amountCheck(control: AbstractControl): ValidationErrors | null {
  if ($('#PaymentMethod').val() !== 'oncredit') {
    return control.value >= +$('.toPay').text() ? null : {error: true};
  } else {
    return null;
  }
}

@Component({
  selector: 'app-receipts',
  templateUrl: './receipts.component.html'
})
export class ReceiptsComponent implements OnInit {

  public defaultCashRegister = null;
  public cashRegisters = [];
  public accounts = [];
  public banks = [];
  public creditCards = [];
  public cashInHand = [];
  public others = [];
  public selectedCashRegister = null;
  public selectedCashRegisterName = null;
  public products: any;
  public company: any;
  public cart = [];
  public FormState = '';
  public subTotal = 0;
  public total = 0;
  public discount = 0;
  public discountType = 'percentage';
  public amountPaid = 0;
  public change = 0;
  public filteredProducts: any;
  public currentUser: User;
  public isOnline = true;
  private subscription: Subscription;
  public locationId: string;
  public TransactionType = 'sale';
  dataForm: FormGroup;
  suppliers: any[];
  public outlets: Store[] = [];
  public warehouses: Store[] = [];
  myStore;
  searchText = '';

  constructor(private route: ActivatedRoute, private datePipe: DatePipe, private fb: FormBuilder, private transporter: TransporterService, private dexieService: DexieService, private activatedRoute: ActivatedRoute, private httpService: SharedHttpService) {
    this.locationId = route.snapshot.params['id'];
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'connection') {
        this.isOnline = parcel.data;
      }
    });
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'supplier' && parcel.data === 'success') {
        this.getUpdatedSuppliers();
      }
    });
  }

  ngOnInit() {
    this.products = [];
    this.suppliers = [];
    this.filteredProducts = this.products.slice(0, 20);
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    if (this.currentUser.Store) {
      this.locationId = this.currentUser.Store;
    }
    this.getMyStore(this.locationId);
    this.getAccounts();
    this.activatedRoute.params.subscribe(params => {
      this.locationId = params['id'];
    });

    this.dataForm = this.fb.group({
      Supplier: [''],
      Comment: [''],
      PaymentMethod: ['Cash'],
      Discount: [0, discountCheck],
      AmountPaid: [''],
      Customer: [''],
      ShippedTo: ['']
    });

    this.activatedRoute.queryParams.subscribe(params => {
      if (params['type'] === 'purchase' || params['type'] === 'sale' || params['type'] === 'transfer') {
        this.TransactionType = params['type'];
      } else {
        this.TransactionType = 'sale';
      }
      if (this.TransactionType === 'sale') {
        this.dataForm.get('AmountPaid').setValidators([Validators.required, amountCheck]);
        this.dataForm.get('PaymentMethod').setValidators(Validators.required);
      } else if (this.TransactionType === 'purchase') {
        this.dataForm.get('Supplier').setValidators(Validators.required);
        this.dataForm.get('AmountPaid').setValidators([Validators.required, amountCheck]);
        this.dataForm.get('PaymentMethod').setValidators(Validators.required);
      } else if (this.TransactionType === 'transfer') {
        this.dataForm.get('ShippedTo').setValidators(Validators.required);
        this.dataForm.get('Discount').clearValidators();
      }
    });

    this.dexieGetCompany();
    this.getUpdatedCompany();
    this.dexieGetProducts();
    this.dexieGetSuppliers();
    this.getUpdatedSuppliers();
    this.getUpdatedProducts();
    this.dexieGetStores();
    if (this.locationId) {
      this.getUpdatedAttributes();
    }
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if ($('.product-card').length) {
      if (event.keyCode === 37 || event.keyCode === 38 || event.keyCode === 39 || event.keyCode === 40) {
        if (!$('.product-card.focused').length) {
          $('.product-card').first().addClass('focused');
          return true;
        }
      }
      if ($('.product-card.focused').length) {
        if (event.keyCode === 37 && $('.product-card.focused').prev().length) {/*left*/
          $('.product-card.focused').removeClass('focused').prev().addClass('focused');
        } else if (event.keyCode === 38) {
          const index = $('.product-card.focused').index() - 3;
          if (index >= 0) {
            $('.product-card.focused').removeClass('focused');
            $('.product-card').eq(index).addClass('focused');
          }
        } else if (event.keyCode === 39 && $('.product-card.focused').next().length) {/*right*/
          $('.product-card.focused').removeClass('focused').next().addClass('focused');
        } else if (event.keyCode === 40) {/*down*/
          const index = $('.product-card.focused').index() + 3;
          if (index < $('.product-card').length) {
            $('.product-card.focused').removeClass('focused');
            $('.product-card').eq(index).addClass('focused');
          }
        } else if (event.keyCode === 13) {/*enter*/
          /*todo: do something on enter*/
          // this.reValidate();
          // $('.product-card.focused').find('button').trigger('click');
        }
      }
    }
  }

  hardRevalidate() {
    this.reValidate();
  }

  getAccounts() {
    let accountsUrl = '/accounts?company=' + this.currentUser.Company;
    this.httpService.Get(accountsUrl)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            for (let a = 0; a < res['body']['data'].length; a++) {
              if (res['body']['data'][a].Store && res['body']['data'][a].Store !== null && res['body']['data'][a].Store._id !== this.locationId) {
                res['body']['data'].splice(a, 1);
              }
            }
            this.accounts = res['body']['data'];
            this.groupAccounts();
          } else {
            this.accounts = [];
          }
        },
        err => {
          if (this.isOnline) {
            console.log('Unexpected error');
          }
        }
      );
  }


  groupAccounts() {
    this.banks = this.accounts.filter( account => {
      if (account.AccountType === 'BankAccount') {
        return account;
      }
    });
    this.cashInHand = this.accounts.filter( account => {
      if (account.AccountType === 'CashInHand') {
        return account;
      }
    });
    this.cashRegisters = this.accounts.filter(account => {
      if (account.AccountType === 'CashDrawer' && account.Store && account.Store._id === this.locationId) {
        return account;
      }
    });
    this.creditCards = this.accounts.filter( account => {
      if (account.AccountType === 'CreditCard') {
        return account;
      }
    });
    this.others = this.accounts.filter( account => {
      if (account.AccountType === 'Other') {
        return account;
      }
    });
  }


  paymentMethodChanged() {
    this.selectedCashRegister = $('#PaymentMethod').val();
    this.selectedCashRegisterName = $('#PaymentMethod option:selected').text();
    this.reValidate();
  }

  selectRegister() {
    this.selectedCashRegister = $('#cashRegister').val();
    this.selectedCashRegisterName = $('#cashRegister option:selected').text();

    this.defaultCashRegister = $('#cashRegister').val();
    this.dataForm.controls['PaymentMethod'].setValue(this.selectedCashRegister);
  }

  resetReceipt() {
    this.cart = [];
    this.discount = this.change = this.amountPaid = this.subTotal = this.total = 0;
    this.dataForm.reset({
      Supplier: '',
      Comment: '',
      PaymentMethod: this.defaultCashRegister,
      Discount: '',
      AmountPaid: 0,
      Customer: '',
      ShippedTo: ''
    });
    if (this.TransactionType === 'sale') {
      this.dataForm.get('AmountPaid').setValidators([Validators.required, amountCheck]);
      this.dataForm.get('PaymentMethod').setValidators(Validators.required);
      this.dataForm.get('Discount').setValidators(discountCheck);
    } else if (this.TransactionType === 'purchase') {
      this.dataForm.get('Supplier').setValidators(Validators.required);
      this.dataForm.get('PaymentMethod').setValidators(Validators.required);
      this.dataForm.get('Discount').setValidators(discountCheck);
      this.dataForm.get('AmountPaid').setValidators([Validators.required, amountCheck]);
    } else if (this.TransactionType === 'transfer') {
      this.dataForm.get('ShippedTo').setValidators(Validators.required);
    }
    this.dataForm.updateValueAndValidity();
  }

  reValidate() {
    this.dataForm.controls['AmountPaid'].updateValueAndValidity();
    this.dataForm.controls['Discount'].updateValueAndValidity();
  }

  submit() {
    if (this.dataForm.valid) {
      const tempUser = (this.currentUser._id).substr(this.currentUser._id.length - 3);
      const tempUUID = (v1()).split('-')[0];
      this.FormState = 'loading';
      this.dataForm.value.LocalId = tempUser + tempUUID;
      this.dataForm.value.Products = this.cart;
      this.dataForm.value.TransactionType = this.TransactionType;
      this.dataForm.value.Company = this.currentUser.Company;
      this.dataForm.value.Store = this.locationId;
      if (this.TransactionType === 'sale') {
        this.dataForm.value.CashDrawer = this.selectedCashRegister;
      }else if (this.TransactionType === 'purchase') {
        this.dataForm.value.PaidFrom = this.selectedCashRegister;
      }
      if (this.discountType === 'flat') {
        this.dataForm.value.Discount = (this.discount / this.subTotal) * 100;
      }

      this.dataForm.value.SalesPerson = this.currentUser._id;
      this.dataForm.value.Date = moment().utcOffset(new Date().getTimezoneOffset());
      if (this.TransactionType === 'sale' || this.TransactionType === 'purchase') {
        this.dataForm.value.SubTotal = this.subTotal;
        this.dataForm.value.Total = this.total;
      }
      if (this.TransactionType === 'purchase' && this.dataForm.value.PaymentMethod === 'oncredit') {
        if (this.dataForm.value.AmountPaid < this.dataForm.value.Total) {
          this.dataForm.value.Payable = this.dataForm.value.Total - this.dataForm.value.AmountPaid;
        }
      }
      if (this.TransactionType === 'transfer') {
        this.dataForm.value.TransactionStatus = 'pending';
      }

      for (let a = 0; a < this.accounts.length; a++) {
        if (this.accounts[a]._id === this.selectedCashRegister) {
          this.dataForm.value.PaymentMethod = this.accounts[a]['AccountType'];
          break;
        }
      }
      console.log(this.dataForm.value.PaymentMethod);
      this.printReceipt();
      this.resetReceipt();
    } else {
      console.log(this.dataForm.valid);
      Object.keys(this.dataForm.controls).forEach(field => {
        const control = this.dataForm.get(field);
        control.markAsDirty({onlySelf: true});
      });
    }
  }

  postReceipt() {
    this.httpService.Post('/receipts', this.dataForm.value)
      .subscribe(res => {
        this.FormState = '';
      }, err => {
        if (this.isOnline) {
          console.log(err);
          alert('Unexpected error, please try posting receipt again.');
        } else {
          alert('Network failure, please post the receipt again.');
        }
      });
  }

  printReceipt() {
    JsBarcode('#pseudo-barcode', this.dataForm.value.LocalId, {
      width: 2,
      height: 50
    });
    const printSize = this.myStore.ReceiptPageSize ? this.myStore.ReceiptPageSize : 'mm80';
    let receiptHTML = '<section class="print-sheet ' + printSize + '">';
    let aboutSupplier = '';
    if (this.TransactionType === 'purchase') {
      aboutSupplier = '<strong>Supplier : ' + $('#Supplier option:selected').text() + '</strong><br>';
    }
    receiptHTML += '<div class="brand-canvas"><h1> ' + this.company['Name'] + ' </h1><h2>' + this.myStore['Name'] + '</h2>' + aboutSupplier
      + this.datePipe.transform(this.dataForm.value.Date, 'dd MMMM,yy h:mm:ss') + '</span></div>';
    receiptHTML += '<table class="receipt-table"><thead><tr><th style="text-align: left">Name</th><th>Price</th><th>Amount</th></tr></thead><tbody>';

    for (let p = 0; p < this.cart.length; p++) {
      receiptHTML += '<tr>';
      receiptHTML += '<td style="text-align:left">' + this.cart[p].Quantity +  ' x ' + this.cart[p].Product.Name + '</td style="text-align: left">';
      if (this.TransactionType === 'purchase') {
        receiptHTML += '<td>' + this.cart[p].Product.Cost + '</td>';
        receiptHTML += '<td>' + this.cart[p].Quantity * this.cart[p].Product.Cost + '</td>';
      } else {
        receiptHTML += '<td>' + this.cart[p].Product.Price + '</td>';
        receiptHTML += '<td>' + this.cart[p].Quantity * this.cart[p].Product.Price + '</td>';
      }
      receiptHTML += '</tr>';
    }
    receiptHTML += '</tbody></table><table class="table-totals">';
    if (this.discount) {
      receiptHTML += '<tr><th>Sub Total</th><td>RS ' + this.subTotal + '</td></tr>';
      receiptHTML += '<tr><th>Discount</th><td>' + this.discount + '</td></tr>';
    }
    receiptHTML += '<tr><th>Grand Total</th><td>RS ' + this.total + '</td></tr>';
    receiptHTML += '</table>';
    receiptHTML += '<div class="barcode">' + $('#pseudo-barcode-wrap').html() + '</div>';
    receiptHTML += '<div class="return_policy">' + this.company.RefundPolicy !== 'undefined' && this.company.RefundPolicy ? this.company.RefundPolicy : '' + '</div>';
    receiptHTML += '<div style="text-align: center; padding: 5pt; margin-top:5px;font-weight:700;border-top: 1px solid #ddd">Thanks for visiting us.</div>';
    receiptHTML += '</section>';
    $('.printable-canvas').html(receiptHTML);
    window.print();
    this.postReceipt();
    this.resetReceipt();
    this.getUpdatedProducts();
  }


  dexieGetCompany() {
    this.dexieService['Company'].where({_id: this.currentUser.Company}).toArray().then(data => {
      this.company = data[0];
    }).catch(err => {
      console.log('err in offline company record.');
    });
  }

  getUpdatedCompany() {
    let lastUpdated;
    if (this.isOnline) {
      this.dexieService['Flag'].where({Name: 'Company'}).toArray().then(data => {
        lastUpdated = data[0];
        this.GetCompany();
      }).catch(err => {
        this.GetCompany();
      });
    }
  }

  GetCompany() {
    this.httpService.Get('/companies/' + this.currentUser.Company)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.dexieService['Company'].put(res['body']).then(data => {
              this.dexieService['Flag'].put({
                Name: 'Company',
                Value: new Date().toJSON()
              }).catch(flagErr => {
                console.log('Failed to put store flag ' + flagErr);
              });
              this.dexieGetCompany();
            }).catch(syncErr => {
              console.log('Failed to put resulted company: ' + syncErr);
            });
          } else {
            console.log('company is up-to-date');
          }
        },
        err => {
          if (this.isOnline) {
            console.log('Unexpected error with status ' + err);
          }
        }
      );
  }

  getMyStore(storeId: string) {
    this.dexieService['Store'].where('_id').equals(storeId).toArray().then(data => {
      this.myStore = data[0];
    }).catch(dexieErr => {
      this.myStore = {};
    });

  }

  setNumber(event, x) {
    if (!(+event.target.value)) {
      event.target.value = +x;
    }
  }

  QtyUpdated(event, product, cartIndex) {
    if (event.keyCode !== 8 || +event.target.value > 0) {
      if (!(+event.target.value)) {
        event.target.value = 1;
      }
      if (this.cart[cartIndex].Product._id === product._id) {
        this.subTotal -= +(this.cart[cartIndex].Quantity * product.Price);
        this.cart[cartIndex].Quantity = +event.target.value;
        this.subTotal += +(this.cart[cartIndex].Quantity * product.Price);
        this.doCalculation();
      }
    }
  }

  QtyUpdatedPurchase(event, product, cartIndex) {
    if (event.keyCode !== 8 || +event.target.value > 0) {
      if (!(+event.target.value)) {
        event.target.value = 1;
      }
      if (this.cart[cartIndex].Product._id === product._id) {
        this.subTotal -= +(this.cart[cartIndex].Quantity * (product.Cost || 0));
        this.cart[cartIndex].Quantity = +event.target.value;
        this.subTotal += +(this.cart[cartIndex].Quantity * (product.Cost || 0));
        this.doCalculation();
      }
    }
  }

  costUpdated(event, product, cartIndex) {
    if (this.cart[cartIndex].Product._id === product._id) {
      this.subTotal -= +(this.cart[cartIndex].Quantity * (product.Cost || 0));
      this.cart[cartIndex].Product.Cost = event.target.value;
      this.subTotal += +(this.cart[cartIndex].Quantity * product.Cost);
      this.doCalculation();
    }
  }

  setDiscountType(e) {
    this.discountType = e.target.value;
    this.discount = 0;
    $('#Discount').val('0');
    this.doCalculation();
  }

  doCalculation() {
    this.discount = +$('#Discount').val() ? +$('#Discount').val() : 0;
    if (this.discountType !== 'flat') {
      this.discount = (this.subTotal / 100) * this.discount;
    }
    this.total = this.subTotal - this.discount;
    this.change = this.amountPaid - this.total;
    this.reValidate();
  }

  setDiscount() {
    this.doCalculation();
  }


  removeProduct(product) {
    if (this.cart.length) {
      for (let p = 0; p < this.cart.length; p++) {
        if (this.cart[p].Product._id === product.Product._id) {
          this.subTotal -= +product.Product.Price * product.Quantity;
          this.cart.splice(p, 1);
          this.doCalculation();
          break;
        }
      }
    } else {
      this.discount = this.change = this.amountPaid = this.subTotal = this.total = 0;
      this.cart = [];
    }
  }

  removeProductFromTransfer(product) {
    if (this.cart.length) {
      for (let p = 0; p < this.cart.length; p++) {
        if (this.cart[p].Product._id === product.Product._id) {
          this.cart.splice(p, 1);
          break;
        }
      }
    } else {
      this.discount = this.change = this.amountPaid = this.subTotal = this.total = 0;
      this.cart = [];
    }
  }

  removeProductPurchase(product) {
    if (this.cart.length) {
      for (let p = 0; p < this.cart.length; p++) {
        if (this.cart[p].Product._id === product.Product._id) {
          this.subTotal -= +(product.Product.Cost || 0) * product.Quantity;
          this.cart.splice(p, 1);
          this.doCalculation();
          break;
        }
      }
    } else {
      this.discount = this.change = this.amountPaid = this.subTotal = this.total = 0;
      this.cart = [];
    }
  }


  setAmountPaid(e) {
    if (e.target.value.length) {
      if (!(+e.target.value)) {
        e.target.value = 0;
      }
      this.amountPaid = e.target.value;
      this.doCalculation();
    }
  }

  /*todo: deal barcode auto add*/
  addToCart(e, product) {
    let isFound = false;
    product.ProductType = product.ProductType ? product.ProductType : '';
    if (product.ProductType.toLowerCase() !== 'deal') {
      if (this.cart.length) {
        for (let p = 0; p < this.cart.length; p++) {
          if (this.cart[p].Product._id === product._id) {
            this.cart[p].Quantity++;
            isFound = true;
          }
        }
      }
      if (!isFound) {
        this.cart.unshift({
          Product: product,
          Quantity: 1
        });
      }
    }else if (e) {
      const tempProduct = jQuery.extend(true, {}, product);
      tempProduct.Name += '<ul>';
      const selectedItems = [];
      const dealOptions = $(e.target).parents('.card').find('.dealOption:checked');
      for (let di = 0; di < dealOptions.length; di++ ) {
        selectedItems.push($(dealOptions[di]).attr('name'));
        tempProduct.Name += '<li>' + $(dealOptions[di]).siblings('span').text() + '</li>';
      }
      tempProduct.SubItems = tempProduct.SubItems.filter( sub => {
        if (selectedItems.indexOf(sub.Product._id) !== -1) {
          return sub;
        }
      });
      tempProduct.Name += '</ul>';
      this.cart.unshift({
        Product: tempProduct,
        Quantity: 1
      });

    }
    this.subTotal += +product.Price;
    this.doCalculation();
    this.reValidate();
  }

  addToCartTransfer(product) {
    let isFound = false;
    if (this.cart.length) {
      for (let p = 0; p < this.cart.length; p++) {
        if (this.cart[p].Product._id === product._id) {
          this.cart[p].Quantity++;
          isFound = true;
        }
      }
    }
    if (!isFound) {
      this.cart.unshift({
        Product: product,
        Quantity: 1
      });
    }
    this.reValidate();
  }

  addToCartPurchase(product) {
    let isFound = false;
    if (this.cart.length) {
      for (let p = 0; p < this.cart.length; p++) {
        if (this.cart[p].Product._id === product._id) {
          this.cart[p].Quantity++;
          isFound = true;
        }
      }
    }
    if (!isFound) {
      this.cart.unshift({
        Product: product,
        Quantity: 1
      });
    }
    this.subTotal += +(product.Cost || 0);
    this.doCalculation();
    this.reValidate();
  }

  dexieGetProducts() {
    this.dexieService['Product'].where({Company: this.currentUser.Company}).toArray()
      .then(data => {
        if (this.TransactionType === 'sale') {
          this.products = data;
        }else {
          this.products = data.filter(product => {
            product.ProductType = product.ProductType ? product.ProductType : 'Single';
            if (product.ProductType.toLowerCase() !== 'deal' && product.ProductType.toLowerCase() !== 'recipe') {
              return product;
            }
          });
        }
        this.filteredProducts = this.products.slice(0, 20);
        this.appendProductAttributes();
      }).catch(err => {
      console.log(err);
      this.products = [];
      this.filteredProducts = this.products;
    });
  }

  getUpdatedProducts() {
    let lastUpdated;
    this.dexieService['Flag'].where({Name: 'Product'}).toArray().then(data => {
      lastUpdated = data[0];
      this.getProducts(lastUpdated['Value']);
    }).catch(err => {
      this.getProducts(false);
    });
  }

  getProducts(updatedAt: any) {
    let queryPart = '';
    if (updatedAt) {
      queryPart = '&updatedAt=' + updatedAt;
    }
    this.httpService.Get('/products?company=' + this.currentUser.Company + queryPart)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.dexieService['Flag'].put({Name: 'Product', Value: new Date().toJSON()}).catch(flagErr => {
              console.log('Failed to put store flag ' + flagErr);
            });
            this.dexieService['Product'].bulkPut(res['body']['data']).then(data => {
              console.log('products synced');
              this.dexieGetProducts();
            }).catch(dexieErr => {
              console.log('Failed to put resulted stores: ' + dexieErr);
            });
          }
        },
        err => {
          if (this.isOnline) {
            console.log('Unexpected error');
          }
        }
      );
  }

  getUpdatedAttributes() {
    if (this.isOnline) {
      let lastUpdated;
      this.dexieService['Flag'].where({Name: 'Attribute'}).toArray().then(data => {
        lastUpdated = data[0];
        this.getAttributes(lastUpdated['Value']);
      }).catch(err => {
        this.getAttributes(false);
      });
    }
  }

  appendProductAttributes() {
    if (this.locationId) {
      for (let p = 0; p < this.products.length; p++) {
        this.dexieService['Attribute'].where({Product: this.products[p]._id}).toArray().then(data => {
          if (data.length) {
            this.products[p].Attribute = data[0];
          } else {
            this.products[p].Attribute = {Stock: 0, Cost: 0};
          }
        }).catch(err => {
          this.products[p].Attribute = {Stock: 0, Cost: 0};
        });
      }
    }
  }

  getAttributes(updatedAt: any) {
    let queryPart = '';
    if (updatedAt) {
      queryPart = '&updatedAt=' + updatedAt;
    }
    this.httpService.Get('/productattributes?store=' + this.locationId + queryPart)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.dexieService['Flag'].put({
              Name: 'Attribute',
              Value: new Date().toJSON()
            }).catch(flagErr => {
              console.log('Failed to put store flag ' + flagErr);
            });
            this.dexieService['Attribute'].bulkPut(res['body']['data']).then(data => {
              console.log('Attributes synced');
              this.appendProductAttributes();
            }).catch(dexieErr => {
              console.log('Failed to put resulted stores: ' + dexieErr);
            });
          }
        },
        err => {
          console.log(err);
          if (this.isOnline) {
            console.log('Unexpected error');
          }
        }
      );
  }


  updateFilter() {
    this.filteredProducts = this.products;
    this.filteredProducts = this.filteredProducts.filter(product => {
      product.ProductType = product.ProductType ? product.ProductType : '';
      if (product.Barcode && product.Barcode.toLowerCase() === this.searchText.toLowerCase()) {
        if (this.TransactionType === 'purchase') {
          this.addToCartPurchase(product);
          this.searchText = '';
        } else if (this.TransactionType === 'sale' && product.ProductType.toLowerCase() !== 'deal') {
          this.addToCart(null, product);
          this.searchText = '';
        } else if (this.TransactionType === 'transfer') {
          this.addToCartTransfer(product);
          this.searchText = '';
        }
      }
      return product.Name.toLowerCase().indexOf(this.searchText) !== -1 || !this.searchText || (product.Barcode && product.Barcode.toLowerCase().indexOf(this.searchText) !== -1);
    }).slice(0, 20);
  }


  newSupplier() {
    this.transporter.transmit({type: 'supplier', data: 'start'});
  }


  dexieGetSuppliers() {
    this.dexieService['Supplier'].where({Company: this.currentUser.Company}).toArray()
      .then(data => {
        this.suppliers = data;
      }).catch(err => {
      this.suppliers = [];
    });
  }

  getUpdatedSuppliers() {
    let lastUpdated;
    this.dexieService['Flag'].where({Name: 'Supplier'}).toArray().then(data => {
      lastUpdated = data[0];
      this.getSuppliers(lastUpdated['Value']);
    }).catch(err => {
      this.getSuppliers(false);
    });
  }

  // swap this out for a subcription to warehouse and outlets observable
  dexieGetStores() {
    this.dexieService['Store'].where({
      StoreType: 'Warehouse',
      Company: this.currentUser.Company
    }).toArray().then(data => {
      for (let index = 0; index < data.length; index++) {
        const store = data[index];
        if (store['_id'] === this.locationId) {
          data.splice(index, 1);
        }
      }
      this.warehouses = data;
    }).catch(err => {
      this.warehouses = [];
    });
    this.dexieService['Store'].where({
      StoreType: 'Outlet',
      Company: this.currentUser.Company
    }).toArray().then(data => {
      for (let index = 0; index < data.length; index++) {
        const store = data[index];
        if (store['_id'] === this.locationId) {
          data.splice(index, 1);
        }
      }
      this.outlets = data;
    }).catch(err => {
      this.outlets = [];
    });
  }

  getSuppliers(updatedAt) {
    let queryPart = '';
    if (updatedAt) {
      queryPart = '&updatedAt=' + updatedAt;
    }
    this.httpService.Get('/suppliers?company=' + this.currentUser.Company + queryPart)
      .subscribe(
        res => {
          if (res['status'] !== 204) {
            this.dexieService['Flag'].put({Name: 'Supplier', Value: new Date().toJSON()}).catch(flagErr => {
              console.log('Failed to put Supplier flag ' + flagErr);
            });
            this.dexieService['Supplier'].bulkPut(res['body']['data']).then(data => {
              this.dexieGetSuppliers();
            }).catch(dexieErr => {
              console.log('Failed to put resulted Supplier: ' + dexieErr);
            });
          }
        },
        err => {
          if (this.isOnline) {
            console.log('Unexpected error');
          }
        }
      );
  }
}
