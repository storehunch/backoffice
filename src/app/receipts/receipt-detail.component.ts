import {User} from './../models/User';
import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import * as $ from 'jquery';
import {SharedHttpService} from '../services/SharedHttp.service';
import {ActivatedRoute} from '@angular/router';
import {v1} from 'uuid';

@Component({
    selector: 'app-receipt-detail',
    templateUrl: './receipt-detail.component.html',
    styles: []
})
export class ReceiptDetailComponent implements OnInit {
    discountForPatch = 0;
    loading = false;
    public receipt = {};
    public ModalState = false;
    public currentUser: User;
    originalReceipt;
    locationId;
    disableEditing = true;
    btnText = 'Return';
    returnReceipt = {
        Products: [],
        ReferenceReceipt: '',
        Store: '',
        SalesPerson: '',
        TransactionType: 'return',
        AmountPaid: 0,
        Date: new Date()
    };
    returnedProducts = [];
    StoreName;
    private subscription: Subscription;

    constructor(private transporter: TransporterService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute) {
        const page = this;
        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'receipt-detail') {
                this.StoreName = parcel.StoreName;
                page.loading = true;
                page.disableEditing = true;
                page.btnText = 'Return';
                page.returnedProducts = [];

                page.receipt = [];
                page.receipt = page.clone(parcel.data);
                let returnType = '';
                if (page.receipt['TransactionType'] === 'sale') {
                    returnType = 'return';
                } else if (page.receipt['TransactionType'] === 'purchase') {
                    returnType = 'returnPurchase';
                }
                page.returnReceipt = {
                    Products: [],
                    ReferenceReceipt: '',
                    Store: '',
                    SalesPerson: '',
                    TransactionType: returnType,
                    AmountPaid: 0,
                    Date: new Date()
                };

                console.log('reciept id' + page.receipt['_id']);
                // subtract product quantities so that the user can not return what is already returned
                page.httpService.Get('/receipts?company=' + page.currentUser.Company + '&store=' + page.locationId + '&ReferenceReceipt=' + page.receipt['_id']).subscribe(data => {

                    if (data) {
                        if (data['status'] === 200) {
                            const result = data['body']['data'];
                            result.forEach(rcpt => {
                                rcpt['Products'].forEach(refProduct => {

                                    page.receipt['Products'].filter(function (p) {
                                        if (p['Product']['_id'] === refProduct['Product']['_id']) {
                                            p['Quantity'] -= refProduct['Quantity'];
                                            console.log(p);
                                        }
                                    });
                                });

                            });

                        }
                    } else {

                    }
                    page.originalReceipt = this.clone(page.receipt); // O.o spooky: clone of object
                    page.discountForPatch = (page.receipt['Discount'] / page.receipt['SubTotal']) * 100;
                    page.loading = false;

                }, err => console.log(err));
                this.ToggleModal();

            }
        });
    }


    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });

    }

    toggleReceiptEditing() {
        if (this.loading) {
            return;
        }
        this.disableEditing = !this.disableEditing;
        this.btnText = 'Return';

        if (!this.disableEditing) {
            this.btnText = 'Confirm Return';

        } else {
            const page = this;

            let paid = 0;
            let proceedWithPost = true; // use this to stop product with 0 qty being added to receipt
            if (this.returnedProducts.length <= 0) {
                proceedWithPost = false;
            }
            this.returnedProducts.forEach(product => {
                if (product['Quantity'] <= 0) {
                    proceedWithPost = false;
                }
                paid += product['Product']['Price'] * product['Quantity'];
            });

            if (!proceedWithPost) {
                return;
            }
            const tempUser = (this.currentUser._id).substr(this.currentUser._id.length - 3);
            const tempUUID = (v1()).split('-')[0];
            this.returnReceipt['AmountPaid'] = paid;
            this.returnReceipt['Products'] = this.returnedProducts;
            this.returnReceipt['ReferenceReceipt'] = this.receipt['_id'];
            this.returnReceipt['Store'] = this.locationId;
            this.returnReceipt['SalesPerson'] = this.currentUser._id;
            this.returnReceipt['Company'] = this.currentUser.Company;
            this.returnReceipt['LocalId'] = tempUser + tempUUID;
            this.returnReceipt.Date = new Date();
            this.httpService.Post('/receipts', this.returnReceipt).subscribe(data => this.ToggleModal(), err => console.log(err));
            this.transport({type: 'receipt-listing', data: 'all'});
        }

        console.log(this.disableEditing);
    }

    addProductToReceipt(checked, product) {
        if (checked) {

            this.returnedProducts.push(product);

        } else {
            this.returnedProducts.splice(this.returnedProducts.indexOf(product), 1);
        }
        console.log(this.returnedProducts);
    }

    patchProductForDiscount() {
        this.originalReceipt.Discount = this.discountForPatch;
        this.httpService.Patch('/receipts/' + this.originalReceipt._id, this.originalReceipt).subscribe(data => console.log('patched for discount'), err => console.log(err))
    }

    changeProductQuantity(event, id) {
        const qty = event.target.value;
        let maxQty;
        const page = this;
        this.originalReceipt['Products'].filter(function (prod) {
            if (prod['Product']['_id'] === id) {
                maxQty = prod['Quantity'];
            }
        });
        this.returnedProducts.filter(function (p) {

            if (p['Product']['_id'] === id) {
                if (qty <= maxQty) {
                    p['Quantity'] = qty;
                } else {
                    event.target.value = maxQty;
                }
            }
        });

    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }
}
