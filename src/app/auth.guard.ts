import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuardGuard implements CanActivate {
    public IsLoggedIn = localStorage.getItem('auth_token') ? true : false;

    constructor(private router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.IsLoggedIn) {
            this.router.navigate(['/login']);
        } else if (JSON.parse(localStorage.getItem('user')).AccessLevel !== 'SuperAdmin') {
            if (state.url === '/') {
                this.router.navigate(['/locations/' + JSON.parse(localStorage.getItem('user')).Store]);
            }
        }
        return this.IsLoggedIn;
    }
}
