import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../models/User';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import {DexieService} from '../services/dexie.service';
import {SharedHttpService} from '../services/SharedHttp.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
import * as moment from 'moment';


@Component({
    selector: 'app-expense',
    templateUrl: './expense.component.html',
    styles: []
})
export class ExpenseComponent implements OnInit, OnDestroy {
    locationId;
    public currentUser:User;
    public isOnline = true;
    public ModalState = false;
    public FormState = '';
    public dataForm:FormGroup;
    public FormErr:String = '';
    accounts = [];
    expenseCategories = [];
    loadingIndicator = false;
    private subscription:Subscription;

    constructor(private httpService:SharedHttpService, private transporter:TransporterService, private dexieService:DexieService, private fb:FormBuilder, private activatedRoute:ActivatedRoute) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'expense') {
                this.getAccounts();
                this.getExpenseCategories();
                if (parcel.data === 'start') {
                    $('body').removeClass('overflow-hidden');
                    this.ToggleModal();
                }
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            if (params['id']) {
                this.locationId = params['id'];
            }
        });
        this.dataForm = this.fb.group({
            Title: ['', Validators.required],
            Amount: ['', Validators.required],
            Company: [this.currentUser['Company']],
            User: [this.currentUser._id],
            Store: [this.locationId],
            ExpenseType: ['', Validators.required],
            Date: [moment().utcOffset(new Date().getTimezoneOffset()), Validators.required],
            TransactionDate: [moment().utcOffset(new Date().getTimezoneOffset())],
            PaidFrom: ['', Validators.required]
        });
    }

    resetForm() {
        this.dataForm.reset({
            Title: '',
            Amount: '',
            Company: this.currentUser['Company'],
            User: this.currentUser._id,
            Store: this.locationId,
            ExpenseType: '',
            Date: new Date(),
            TransactionDate: new Date(),
            PaidFrom: ['', Validators.required]
        });
        this.dataForm.get('Title').setValidators([Validators.required]);
        this.dataForm.get('Amount').setValidators(Validators.required);
        this.dataForm.get('Date').setValidators(Validators.required);
        this.dataForm.get('ExpenseType').setValidators(Validators.required);
        this.dataForm.updateValueAndValidity();
    }

    submit() {
        console.log(this.dataForm.value);
        console.log('submit');
        if (this.dataForm.valid) {
            this.FormState = 'loading';
            this.PostExpense();
        } else {
            console.log(this.dataForm.value);
            console.log('not valid');
            Object.keys(this.dataForm.controls).forEach(field => {
                const control = this.dataForm.get(field);
                control.markAsDirty({onlySelf: true});
            });
        }
    }

    getExpenseCategories() {
        this.httpService.Get('/expense-categories?company='+this.currentUser.Company).subscribe(res => {
            this.expenseCategories = res['body']['data'];
            console.log('ex cat', this.expenseCategories);
        }, err => {
        });
    }

    PostExpense() {
        this.httpService.Post('/expenses', this.dataForm.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.resetForm();
                    this.transport({type: 'expense', data: 'success'});
                    this.transporter.transmit({type: 'get-transactions', data: true});
                    this.transporter.transmit({type: 'get-accounts', data: true});
                    this.ToggleModal();
                },
                err => {
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }
                        }
                    }
                }
            );
    }

    /*
     * execute these line on expense success
     * this.transport({type: 'expense', data: 'success'});
     * this.ToggleModal();
     * */

    getAccounts() {
        let accountsUrl = '/accounts?company=' + this.currentUser.Company;
        accountsUrl = this.currentUser.Store ? accountsUrl + '&store=' + this.currentUser.Store : accountsUrl;
        this.httpService.Get(accountsUrl)
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.accounts = res['body']['data'];
                        console.log('accounts', this.accounts);
                    } else {
                        this.accounts = [];
                    }
                    this.loadingIndicator = false;
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error');
                    }
                    this.loadingIndicator = false;
                }
            );

    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    transport(packet:any):void {
        this.transporter.transmit(packet);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
