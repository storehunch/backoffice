import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {User} from "../models/User";
import {SharedHttpService} from "../services/SharedHttp.service";
import {TransporterService} from "../services/transporter.service";
import {DatatableComponent} from "@swimlane/ngx-datatable";

declare var google: any;


@Component({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styles: []
})
export class AnalyticsComponent implements OnInit {

    public title = 'Reports';
    public printHTML = '';
    public Expenses = [];
    public Receipts = [];
    public Taxonomies;
    public CategoryWiseSalesReport;
    public LocationId: string;
    public DateWiseSalesJournal = [];
    CurrentUser: User;
    Totals = [];
    public date_from;
    public date_to;
    public IsReady = false;
    loadingIndicator = true;
    cumilativeStockReport = [];
    products = [];
    subscription;
    ExpenseReport = [];
    index = 0;
    reportsTypeFilter = [true, false, false, false, false]; // 0 Profit Loss Statement, 1 Date Wise Sales Journal , 2 Category Wise Sales Report, 3 Cumilative Stock Sales Report,4 Product Summary

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {
    }

    ngOnInit() {
        this.CurrentUser = this.CurrentUser = JSON.parse(localStorage.getItem('user'));
        this.Expenses = [];
        this.Receipts = [];
        this.Taxonomies = [];
        this.CategoryWiseSalesReport = [];

        this.activatedRoute.params.subscribe(params => {
            this.LocationId = this.LocationId = params['id'];
            this.IsReady = false;
            this.getMetrics();
        });

    }

    setDateFrom(df) {

        this.date_from = new Date(df);
        // if (isNaN(this.date_from)) {
        //     this.date_from = '';
        // }
        this.getMetrics();

    }

    setDateTo(dt) {

        this.date_to = new Date(dt);
        // if (isNaN(this.date_to)) {
        //     this.date_to = '';
        // }
        this.getMetrics();
    }

    public getMetrics() {
        let query = '/reports?company=' + this.CurrentUser.Company + '&stores=' + this.LocationId;
        if (this.date_from) {
            query += '&date_from=' + this.date_from.toLocaleDateString().split('T')[0];
        }
        if (this.date_to) {
            query += '&date_to=' + this.date_to.toLocaleDateString().split('T')[0];

        } else if (this.date_from) {
            query += '&date_to=' + (this.date_from.getMonth() + 1) + '/' + (this.date_from.getDay() + 3) + '/' + this.date_from.getFullYear();

        }
        this.httpService.Get(query)
            .subscribe(
                res => {
                    console.log('res', res);

                    this.Totals = [res['body']['totals']];
                    this.Expenses = res['body']['expenses'];
                    this.ExpenseReport = res['body']['expenseReport'];
                    this.CategoryWiseSalesReport = res['body']['categoryWiseSalesReport'];
                    this.DateWiseSalesJournal = res['body']['dateWiseSalesJournal'];
                    const grossEarning = this.Totals[0].Profit - this.Totals[0].Expense;

                    this.transporter.transmit({
                        type: 'profit-loss-statement',
                        Expenses: this.Expenses,
                        GrossEarning: grossEarning
                    });
                    this.makeDailyCharts();
                    this.makeCategoryWiseRevenueandProfitChart();
                    this.makeCategoryWiseSalesChart();
                    this.makeExpenseCharts();
                    this.loadingIndicator = false;
                },
                err => {
                    console.log('Unexpected error, Please contact support.');
                    this.loadingIndicator = false;

                }
            );
    }

    reportsTypeChanged(index: number) { // index must be number so as not to mess with array indexes

        if (index === undefined) {
            return;
        } // keep this
        this.reportsTypeFilter = [false, false, false, false, false];
        this.reportsTypeFilter[index] = true;
    }

    makeCategoryWiseSalesChart() {
        const page = this;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            let dataArray = [];
            dataArray.push(['Sales', 'Category']);
            page.CategoryWiseSalesReport.forEach(dp => {
                if (!dp.Name || dp.Name.length < 1) {
                    dp.Name = 'Other';
                }
                dataArray.push([dp.Name, dp.Sales]);
            });


            let data = google.visualization.arrayToDataTable(dataArray);


            const options = {

                title: 'Sales By Category',
                pieHole: 0.4, backgroundColor: {
                    fill: '#fafafa',
                    fillOpacity: 0.8
                }


            };

            const chart = new google.visualization.PieChart(document.getElementById('categoryWisesalesChart'));
            chart.draw(data, options);
        }


    }

    makeCategoryWiseRevenueandProfitChart() {
        const page = this;
        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            let dataArray = [];
            dataArray.push(['Category', 'Revenue', 'Profit']);
            page.CategoryWiseSalesReport.forEach(lr => {
                if (lr.OS !== 'Unknown' && lr.Profit >= 1 && lr.Name && lr.Name.length && lr.Name.length > 0) {
                    dataArray.push([lr.Name, lr.Revenue, lr.Profit]);
                }
            });

            let data = google.visualization.arrayToDataTable(dataArray);


            let options = {


                chart: {
                    title: 'Sales By Category',
                    subtitle: 'Sales Revenue & Profit'
                }, bars: 'vertical', colors: ['#1bb934', '#6200EA', '#3F51B5'], backgroundColor: {
                    fill: '#fafafa',
                    fillOpacity: 0.8
                }

            };

            let chart = new google.charts.Bar(document.getElementById('categoryChart'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }


    }


    makeExpenseCharts() {
        const page = this;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            let dataArray = [];
            dataArray.push(['Name', 'Expense']);
            page.ExpenseReport.forEach(lr => {
                dataArray.push([lr.Name, lr.Amount]);
            });

            let data = google.visualization.arrayToDataTable(dataArray);


            const options = {

                title: 'Expense By Category',
                pieHole: 0.4, backgroundColor: {
                    fill: '#fafafa',
                    fillOpacity: 0.8
                }


            };

            const chart = new google.visualization.PieChart(document.getElementById('expenseChart'));
            chart.draw(data, options);
        }


    }


    makeDailyCharts() {
        const page = this;
        google.charts.load('current', {'packages': ['line']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            // noinspection TsLint
            let dataArray = [];

            dataArray.push(['Date', 'Gross Earnings', 'Profit', 'Revenue', 'Expenses', 'Discounts']);
            page.DateWiseSalesJournal.forEach(reportObj => {

                // noinspection TypeScriptValidateTypes
                dataArray.push([reportObj.Date, reportObj.GrossEarning, reportObj.Profit, reportObj.Revenue, reportObj.Expense, reportObj.Discounts]);
            });

            const data = google.visualization.arrayToDataTable(dataArray);


            const options = {
                legend: {position: 'right'},
                chart: {
                    title: 'Daily'
                },
                colors: ['#2ea1f8', '#1bb934', 'grey', '#6200EA', '#3F51B5'],
                height: 500, backgroundColor: {
                    fill: '#fafafa',
                    fillOpacity: 0.8
                }

            };

            const chart = new google.charts.Line(document.getElementById('dailySalesChart'));

            chart.draw(data, google.charts.Line.convertOptions(options));
        }
    }
}
