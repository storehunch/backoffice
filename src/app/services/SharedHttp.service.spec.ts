import { TestBed, inject } from '@angular/core/testing';

import { SharedHttpService } from './SharedHttp.service';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedHttpService]
    });
  });

  it('should be created', inject([SharedHttpService], (service: SharedHttpService) => {
    expect(service).toBeTruthy();
  }));
});
