import {Injectable} from '@angular/core';
import * as $ from "jquery";
import {DexieService} from "./dexie.service";

@Injectable()
export class PrintService {
    rows = [];
    columns = [];
    CurrentUser;
    myStore;

    constructor(private dexieService: DexieService) {
        this.CurrentUser = JSON.parse(localStorage.getItem('user'));

    }

    public printReport(data, reportName, storeId, extraHtml = '', heads = ['']) {
        // this.dexieService['Store'].where('_id').equals(storeId).toArray().then(data => {
        //     this.myStore = data[0];
        // }).catch(dexieErr => {
        //     this.myStore = {};
        // });
        let html = '<!DOCTYPE html>\n' +
            '<html style="width: 100%">\n' + ' <head> <title>' + reportName + '</title></head>' +

            '<body style="width: 100%;">'

        html += '<table style="width: 100%;text-align: left;margin: 20pt 0;">' + '<tr><th style="width: 200pt">Report Type</th><td>' + reportName + '</td></tr>' + '<tr><th>Date</th><td>' + new Date().toLocaleDateString() + '</td></tr>' + '<tr><th>Time</th><td>' + new Date().toLocaleTimeString() + '</td></tr>' + '<tr><th>Staff</th><td>' + this.CurrentUser.FirstName + ' ' + this.CurrentUser.LastName + '</td></tr>' + extraHtml + '</table>'
        let index = 0;
        data.forEach(table => {

            console.log(heads)
            html += '<h2>' + heads[index] + '</h2>'
            index += 1
            this.rows = [];
            this.columns = [];

            const arraySchemaObject = table[0];
            for (const key in arraySchemaObject) {
                if (!arraySchemaObject.hasOwnProperty(key)) {
                    // The current property is not a direct property of p, i.e not iterate-able
                    continue;
                }
                this.columns.push(key);

            }
            this.rows = table;

            // '<tr><th>Location</th><td>' + this.myStore.Name + '</td></tr>' for location thing
            html += '<table style="border-collapse: collapse;width: 100%"><tr>';
            this.columns.forEach(column => {
                html += '<th style="border: 1pt solid black;padding: 6pt;text-align: left">' + column.replace(/([A-Z])/g, ' $1').trim() + '</th>';
            });
            html += '</tr>'
            this.rows.forEach(row => {
                html += '<tr>';
                this.columns.forEach(column => {
                    let val = row[column];
                    if (typeof val === 'string') {
                        val = this.insertSpaces(val);
                    }
                    html += '<td style="border: 1pt solid black;padding: 3pt">' + val + '</td>';
                });
                html += '</tr>';
            });
            html += ' </table>\n';
        });

        html += '</body></html>';
        const w = window.open();

        $(w.document.body).html(html);


    }

    insertSpaces(string) {
        string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
        string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
        return string;
    }

}
