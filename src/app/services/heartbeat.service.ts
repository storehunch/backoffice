import {Injectable} from '@angular/core';
import {Socket} from 'ng-socket-io';

@Injectable()
export class HeartbeatService {

    constructor(private socket: Socket) {
    }

    heartBeat(): boolean {
        return this.socket.emit('heartbeat', 'beat', callback => {
            return callback;
        });
    }

}
