import {Injectable} from '@angular/core';
import {SharedHttpService} from './SharedHttp.service';
import {TransporterService} from './transporter.service';
import * as AWS from '../../../node_modules/aws-sdk';

// import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
@Injectable()
export class StorageService {
    s3;
    albumBucketName = 'storehunch-images';
    bucketRegion = 'us-west-2';
    IdentityPoolId = 'us-west-2:f7e13c74-3d65-41cb-80f1-2b1cbcce9f32';
    baseUrl = 'https://s3-us-west-2.amazonaws.com/storehunch-images/';

    constructor(private httpService: SharedHttpService, private transporterService: TransporterService) {



        // noinspection TypeScriptValidateTypes
        const myCredentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: this.IdentityPoolId});
        const myConfig = new AWS.Config({
            credentials: myCredentials, region: this.bucketRegion
        });
        // noinspection TypeScriptValidateTypes
        AWS.config.update({
            region: this.bucketRegion,
            credentials: new AWS.CognitoIdentityCredentials({
                IdentityPoolId: this.IdentityPoolId
            })
        });

        this.s3 = new AWS.S3({
            apiVersion: '2006-03-01',
            params: {Bucket: this.albumBucketName}
        });

    }

    public addFile(file, path=null,callback: Function) {
        // this.spinnerService.show();
        if (!file) {
            return alert('Please choose a file to upload first.');
        }
        const photoKey = file.name.replace(/\s/g, ''); // its the file name

        this.s3.upload({
            Key: photoKey,
            Body: file,
            ACL: 'public-read'
        }, (err, data) => {
            if (err) {
                return alert('There was an error uploading your File: ' + err.message);
            }
            // this.spinnerService.hide();
            callback(this.baseUrl + photoKey,path);
        });


    }

    public deleteFile(url,callback: Function) {
        const splitUrl = url.split('/');
        const photoKey = splitUrl[splitUrl.length - 1];
        this.s3.deleteObject({Key: photoKey}, function (err, data) {
            if (err) {
                return alert('There was an error deleting your File: ' + err.message);
            }

            callback();
            //alert('Successfully deleted File.');
            return ;
        });
    }

}
