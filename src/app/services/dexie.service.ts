import {Injectable} from '@angular/core';
import Dexie from 'dexie';

@Injectable()
export class DexieService extends Dexie {
    constructor() {
        super('storehunch');
        this.version(1).stores({
            Flag: 'Name,Value',
            Company: '_id,__v,Name,Logo,RefundPolicy,AnalyticalReports,LoyaltyProgramsEnabled,EmailsEnabled,LabelPrinting,SMSEnabled,IngredientsEnabled,SidesEnabled,ExtrasEnabled,MultipleAccountsEnabled,CreditCardProcessing,FullAnalyticalReports,MaxProducts,MaxEmailsPerDay,MaxAccounts,Referrer',
            User: '_id,__v,FirstName,LastName,Picture,Username,Store,Company,Email,Salary,MobileNumber,Password,Street,City,State,Country,PostalCode,AccessLevel,UserType,Verified',
            Store: '_id,__v,Name,Phone,Email,Street,City,State,Country,Postal,Lat,Long,Currency,ReceiptPrinter,ReceiptPageSize,ReceiptsToPrint,[StoreType+Company]',
            Product: '_id,__v,Images,Price,Discount,Company,IsFeatured,IsOnSale,ProductType,SubItems,MeasurementScale',
            Attribute: '_id,__v,[Product+Store],Cost,Stock',
            Supplier: '_id,__v,Name,Company,Phone,Address,Email',
            Expense: '_id,__v,Title,Company,Store,User,Extra,Amount,DateTime',
            DayClosing: '_id,__v,Date,Company,Store,OpeningCash,ClosingCash,LocalId,Revenue,Profit,Loss,Sales,Returns,User',
            Taxonomy: '_id,__v,Name,Parent,IsCategory,Company',
            Receipt: '_id,__v,TransactionType,PaymentMethod,LocalId,Date,Store,Company,SalesPerson,SubTotal,Discount,Tax,Total,AmountPaid,TransactionStatus,Person,ShippedTo,Products,Comment',
            OfflineReceipt: '_id,__v,TransactionType,PaymentMethod,LocalId,Date,Store,Company,SalesPerson,SubTotal,Discount,Tax,Total,AmountPaid,TransactionStatus,Person,ShippedTo,Products,Comment'
        });
    }
}
