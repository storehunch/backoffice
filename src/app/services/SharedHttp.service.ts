import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
import {environment} from '../../environments/environment';



@Injectable()
export class SharedHttpService {


    private apiBaseUrl;

    constructor(private http: HttpClient) {
        this.apiBaseUrl = environment.apiUrl;
    }

    Get (EndPoint: String): Observable<any[]> {
        return this.http.get<any[]>(this.apiBaseUrl + EndPoint, {observe: 'response', headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token' :  localStorage.getItem('auth_token') })})
            .pipe(
                map((res) => {
                    return {status: res.status, body: res.body ? res.body : [] };
                }),
                /*tap(data => console.log(data)),*/
                catchError(this.handleError('Get at ' + this.apiBaseUrl + EndPoint))
            );
    }

    Delete (EndPoint: String): Observable<any[]> {
        return this.http.delete<any[]>(this.apiBaseUrl + EndPoint, {observe: 'response', headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token' :  localStorage.getItem('auth_token') })})
            .pipe(
                map((res) => {
                    return {status: res.status, body: res.body ? res.body : [] };
                }),
                /*tap(data => console.log(data)),*/
                catchError(this.handleError('Delete at ' + this.apiBaseUrl + EndPoint))
            );
    }
    Authenticate (credentials: any): Observable<any[]> {
        return this.http.post<any>(this.apiBaseUrl + '/authenticate', credentials ).pipe(
            /*tap(data => console.log('success')),*/
            catchError(this.handleError('Authenticate'))
        );
    }

    Post (EndPoint: String , FormData: any): Observable<any[]> {
        return this.http.post<any>(this.apiBaseUrl + EndPoint, FormData , {observe: 'response', headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token' :  localStorage.getItem('auth_token') })}).pipe(
            map((res) => {
                return {status: res.status, body: res.body };
            }),
            /*tap(data => console.log('success')),*/
            catchError(this.handleError('POST at ' + this.apiBaseUrl + EndPoint ))
        );
    }

    Patch (EndPoint: String , FormData: any): Observable<any[]> {
        return this.http.patch<any>(this.apiBaseUrl + EndPoint, FormData , {headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token' :  localStorage.getItem('auth_token') })}).pipe(
            /*tap(data => console.log('success')),*/
            catchError(this.handleError('Patch at ' + this.apiBaseUrl + EndPoint ))
        );
    }
    private handleError(operation: String) {
        return (err: any) => {
          console.log(`Error in Operation ` + operation , err);
            if (err.status === 401) {
                /*localStorage.clear();
                this.router.navigate(['/login']);*/
                console.log('401 here');
            }else {
                return Observable.throw(err);
            }
        };
    }


}


