import { Injectable } from '@angular/core';
import { DexieService } from './dexie.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DexieCrudService {

  constructor(private dexieService: DexieService) { }


    Post (table: any, FormData: any): Observable<any[]> {
        return this.dexieService[table].add(FormData).then(() => {
            console.log('successfully added');
        }).catch ( err => {
            console.log('Failed to add ' + table + ' into DB: ' + err);
        });
    }


    getAll(table) {
        return this.dexieService[table].toArray();
    }

    query(table, query) {
        return this.dexieService[table].where(query).toArray().then(data => {
            return data;
        }).catch(err => {
            console.log('Failed to query in ' + table + ' with error: ' +  err.name);
        });
    }

    queryCount(table, query) {
        return this.dexieService[table].where(query).count().then(data => data).catch(err => {
            console.log('Failed to query in ' + table + ' with error: ' +  err.name);
        });
    }

    put(table, data) {
        return this.dexieService[table].put(data).then( result => result).catch ( err => {
            console.log('Failed to add ' + table + ' into DB: ' + err);
        });
    }

    putBulk(table, data) {
        return this.dexieService[table].bulkPut(data).then( result => result).catch ( err => {
            console.log('Failed to add ' + table + ' into DB: ' + err);
        });
    }

    update(table, id, data) {
        return this.dexieService[table].update(id, data).then( result => result).catch ( err => {
            console.log('Failed to add ' + table + ' into DB: ' + err);
        });
    }

    remove(table, id) {
        return this.dexieService[table].delete(id).then( result => result).catch ( err => {
            console.log('Failed to add ' + table + ' into DB: ' + err);
        });
    }

}
