import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {SharedHttpService} from '../../services/SharedHttp.service';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from "../../services/transporter.service";
import {User} from '../../models/User';

@Component({
  selector: 'app-new-customer',
  templateUrl: './new-customer.component.html',
  styles: []
})
export class NewCustomerComponent implements OnInit {

  public dataForm: FormGroup;
    public responseType = 'd-none';
    public FormErr: String = '';
    public FormState: String = '';
  public ModalState = false;
  public exitAfterSave = true;
  public currentUser: User;
  private subscription: Subscription;

  constructor( private transporter: TransporterService, private fb: FormBuilder, private httpService: SharedHttpService) {
    this.subscription = this.transporter.receive().subscribe(parcel => {
      if (parcel.type === 'new-customer') {
        if (parcel.data === 'start') {
          $('body').removeClass('overflow-hidden');
          this.ToggleModal();
        }
      }
    });


  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.dataForm = this.fb.group({
      Company: [this.currentUser['Company']],
      Salutation: ['Mr'],
      FirstName: [''],
      LastName: [''],
      Email: [''],
      MobileNumber: ['', Validators.required]
    });
  }

  ToggleModal() {
    $('body').toggleClass('overflow-hidden');
    return this.ModalState = !this.ModalState;
  }

    resetForm() {
        this.dataForm.reset({
            Salutation: 'Mr',
            FirstName: '',
            LastName: '',
            Email: '',
            MobileNumber: '',
            Company: this.currentUser['Company']
        });
        this.dataForm.get('MobileNumber').setValidators(Validators.required);
        if (this.exitAfterSave) {
            this.ToggleModal();
        }
    }



    submit(exit) {
        this.exitAfterSave = exit;
        console.log(this.dataForm.value);
        if(this.dataForm.value.Email === '' || this.dataForm.value.Email === null){
            delete this.dataForm.value.Email;
        }

        console.log(this.dataForm.value);
        this.httpService.Post('/customers', this.dataForm.value)
            .subscribe(
                data => {
                    console.log(data);
                    this.FormState = '';
                    this.responseType = 'success';
                    this.FormErr = 'Customer added successfully.';
                    this.transporter.transmit({type: 'get-customers', data: true});
                    setTimeout(() => {
                        this.responseType = 'd-none';
                    }, 3000);
                    this.resetForm();
                },
                err => {
                    if (err.error) {
                        const data = err.error.errors;
                        this.FormState = '';
                        this.responseType = 'error';
                        for (const key of Object.keys(data)) {
                            if (key !== 'Company') {
                                if (data[key].kind === 'required') {
                                    this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                                } else {
                                    if (key === 'CompanyBarcode') {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Barcode</strong>  already exists.</div>`;
                                    } else {
                                        this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                                    }
                                }
                            }
                        }
                        setTimeout(() => {
                            this.responseType = 'd-none';
                        }, 4000);
                        this.dataForm.reset(this.dataForm.value);
                    }
                }
            );
    }
}
