import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../models/User';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {SharedHttpService} from "../services/SharedHttp.service";

@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styles: []
})
export class CustomersComponent implements OnInit {

    rows = [];
    public customers = [];
    public currentUser:User;
    public isOnline = true;
    public locationId:string;
    loadingIndicator = true;
    @ViewChild(DatatableComponent) table:DatatableComponent;
    private subscription:Subscription;

    constructor(private transporter:TransporterService, private activatedRoute:ActivatedRoute, private httpService:SharedHttpService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }

            /*      if (parcel.type === 'products') {
             this.products = parcel.data;
             this.rows = this.products;
             this.loadingIndicator = false;
             }*/

            if (parcel.type === 'get-customers') {
                this.loadingIndicator = true;
                this.getCustomers();
            }


        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });

        this.getCustomers();
    }


    getCustomers() {
        this.httpService.Get('/company-customers?company=' + this.currentUser.Company)
            .subscribe(
                res => {
                    this.loadingIndicator = false;
                    if (res['status'] !== 204) {
                        console.log('customers', res['body']['data']);
                        this.rows = this.customers = res['body']['data'];

                    } else {
                        console.log('Customers are up-to-date');
                    }
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error with status ' + err.status);
                    }
                }
            );
    }

   /* deleteCustomer(id:any) {
        this.httpService.Delete('/company-customers/' + id).subscribe(res => {
            this.getCustomers();
        }, err => {
            console.log(err);
        })

    }*/

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        this.rows = this.customers.filter(x=>(x.Customer.MobileNumber && x.Customer.MobileNumber.includes(val)) || (x.Customer.FirstName && x.Customer.FirstName.includes(val)) );


        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    transport(packet:any):void {
        this.transporter.transmit(packet);
    }


    triggerAddCustomer() {
        $('body').removeClass('overflow-hidden');
        this.transport({type: 'new-customer', data: 'start'});
    }

}

