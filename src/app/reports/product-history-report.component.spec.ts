import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductHistoryReportComponent } from './product-history-report.component';

describe('ProductHistoryReportComponent', () => {
  let component: ProductHistoryReportComponent;
  let fixture: ComponentFixture<ProductHistoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductHistoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductHistoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
