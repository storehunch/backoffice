import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import '@fengyuanchen/datepicker';

import {SharedHttpService} from '../services/SharedHttp.service';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-category-wise-sales-report',
    templateUrl: './category-wise-sales-report.component.html',
    styles: []
})
export class CategoryWiseSalesReportComponent implements OnInit {


    public CategoryWiseSalesReport;

    rows;
    loadingIndicator = true;
    locationId;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private subscription: Subscription;

    constructor(private printService: PrintService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'category-wise-sales-report') {

                this.locationId = parcel.locationId;
                this.rows = this.CategoryWiseSalesReport = parcel.CategoryWiseSalesReport;
                this.loadingIndicator = false;
            }
        });
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        this.rows = this.CategoryWiseSalesReport;
        // filter our data
        this.rows = this.rows.filter(function (d) {
            return d.Name.toLowerCase().indexOf(val) !== -1 || !val;
        });


    }

    ngOnInit(): void {

    }

    getRowHeight(row) {
        return row.height;
    }

    printReport() {
        let temp = this.clone(this.rows);

        const mappedRows = temp.map(function (item) {
            delete item['_id'];
            return item;
        });
        this.printService.printReport([mappedRows], 'Category Wise Sales Report', this.locationId);
    }
    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }



}
