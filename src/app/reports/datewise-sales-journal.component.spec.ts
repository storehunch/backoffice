import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatewiseSalesJournalComponent } from './datewise-sales-journal.component';

describe('DatewiseSalesJournalComponent', () => {
  let component: DatewiseSalesJournalComponent;
  let fixture: ComponentFixture<DatewiseSalesJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatewiseSalesJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatewiseSalesJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
