import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as $ from 'jquery';
import '@fengyuanchen/datepicker';

import { SharedHttpService } from '../services/SharedHttp.service';
import { User } from '../models/User';

@Component({
  selector: 'app-product-history-report',
  templateUrl: './product-history-report.component.html',
  styles: []
})
export class ProductHistoryReportComponent implements OnInit {

  constructor(private httpService: SharedHttpService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }


  getRowHeight(row) {
    return row.height;
  }

}
