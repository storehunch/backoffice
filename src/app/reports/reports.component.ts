import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import '@fengyuanchen/datepicker';

import {SharedHttpService} from '../services/SharedHttp.service';
import {User} from '../models/User';
import {TransporterService} from '../services/transporter.service';
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styles: []
})
export class ReportsComponent implements OnInit {

    public title = 'Reports';
    public printHTML = '';
    public Expenses = [];
    public Receipts = [];
    public Taxonomies;
    public CategoryWiseSalesReport;
    public LocationId: string;
    public DateWiseSalesJournal = [];
    CurrentUser: User;
    Totals = {};
    public date_from;
    public date_to;
    public IsReady = false;
    loadingIndicator = true;
    cumilativeStockReport = [];
    products = [];
    subscription;
    ExpenseReport = [];
    index = 0;
    totalsToBind = [];
    dateRange;
    reportsTypeFilter = [true, false, false, false, false, false]; // 0 Profit Loss Statement, 1 Date Wise Sales Journal , 2 Category Wise Sales Report, 3 Cumilative Stock Sales Report,4 Product Summary

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private printService: PrintService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {
    }

    ngOnInit() {
        this.CurrentUser = this.CurrentUser = JSON.parse(localStorage.getItem('user'));
        this.Expenses = [];
        this.Receipts = [];
        this.Taxonomies = [];
        this.CategoryWiseSalesReport = [];

        this.activatedRoute.params.subscribe(params => {
            this.LocationId = this.LocationId = params['id'];
            this.IsReady = false;
            this.getMetrics();
        });

    }

    public getMetrics() {
        let query = '/reports?company=' + this.CurrentUser.Company + '&stores=' + this.LocationId;
        if (this.date_from) {
            query += '&date_from=' + this.date_from.toLocaleDateString().split('T')[0];
        }
        if (this.date_to) {
            query += '&date_to=' + this.date_to.toLocaleDateString().split('T')[0];

        } else if (this.date_from) {
            query += '&date_to=' + (this.date_from.getMonth() + 1) + '/' + (this.date_from.getDay() + 3) + '/' + this.date_from.getFullYear();

        }
        this.httpService.Get(query)
            .subscribe(
                res => {
                    console.log('res', res);
                    this.transporter.transmit({
                        type: 'datewise-sales-journal',
                        DateWiseSalesJournal: res['body']['dateWiseSalesJournal'], locationId: this.LocationId
                    });
                    this.transporter.transmit({
                        type: 'cumilative-stock-report',
                        CumilativeStockReport: res['body']['cumilativeStockReport'], locationId: this.LocationId
                    });
                    this.transporter.transmit({
                        type: 'category-wise-sales-report',
                        CategoryWiseSalesReport: res['body']['categoryWiseSalesReport'], locationId: this.LocationId
                    });
                    this.Totals = res['body']['totals'];
                    let temp = [];
                    for (const key in this.Totals) {
                        if (this.Totals.hasOwnProperty(key)) {
                            temp.push({Head: key, Amount: this.Totals[key]});

                        }

                    }
                    this.totalsToBind = temp;
                    this.Expenses = res['body']['expenses'];
                    this.ExpenseReport = res['body']['ExpenseReport'];
                    this.DateWiseSalesJournal = res['body']['dateWiseSalesJournal'];
                    this.dateRange = res['body']['Date'];

                    this.transporter.transmit({
                        type: 'profit-loss-statement',
                        Expenses: this.Expenses,
                        GrossEarning: this.Totals['GrossEarning'], totals: this.totalsToBind
                    });


                    console.log('totals now', this.totalsToBind);
                    this.loadingIndicator = false
                },
                err => {
                    console.log('Unexpected error, Please contact support.');
                    this.loadingIndicator = false;

                }
            );
    }

    printReport() {

        this.printService.printReport([this.totalsToBind], 'Sale and Purchase Report', this.LocationId);
    }

    reportsTypeChanged(index: number) { // index must be number so as not to mess with array indexes

        if (index === undefined) {
            return;
        } // keep this
        this.reportsTypeFilter = [false, false, false, false, false, false];
        this.reportsTypeFilter[index] = true;
    }

    setDateFrom(df) {

        this.date_from = new Date(df);

        this.getMetrics();

    }

    setDateTo(dt) {

        this.date_to = new Date(dt);

        this.getMetrics();
    }


}
