import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryWiseSalesReportComponent } from './category-wise-sales-report.component';

describe('CategoryWiseSalesReportComponent', () => {
  let component: CategoryWiseSalesReportComponent;
  let fixture: ComponentFixture<CategoryWiseSalesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryWiseSalesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryWiseSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
