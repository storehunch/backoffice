import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import '@fengyuanchen/datepicker';

import {SharedHttpService} from '../services/SharedHttp.service';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-profit-loss-statement',
    templateUrl: './profit-loss-statement.component.html',
    styles: []
})
export class ProfitLossStatementComponent implements OnInit {

    public Expenses = [];
    locationId;
    GrossEarning = 0;
    loadingIndicator = true;
    totals;
    private subscription: Subscription;

    constructor(private printService: PrintService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'profit-loss-statement') {
                this.locationId = parcel.locationId;
                this.totals = parcel.totals;
                this.Expenses = parcel.Expenses;
                this.GrossEarning = parcel.GrossEarning;

                this.loadingIndicator = false;


            }
        });
    }

    printReport() {
        let temp = this.clone(this.Expenses);

        const mappedRows = temp.map(function (item) {
            const mDate = item['Date'].split('T')[0].split('-')[2] + '-' + item['Date'].split('T')[0].split('-')[1] + '-' + item['Date'].split('T')[0].split('-')[0];
            item['Date'] = mDate;
            delete item['_id'];
            delete item['Company'];
            delete item['User'];
            delete item['Store'];
            delete item['ExpenseType'];
            delete item['createdAt'];
            delete item['updatedAt'];
            delete item['__v'];

            return item;
        });
        this.printService.printReport([this.totals, mappedRows], 'Expenses', this.locationId, '', ['Gross Earning : ' + this.GrossEarning.toFixed(1), 'Expenses']);
    }

    ngOnInit() {
    }

    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }

    getRowHeight(row) {
        return row.height;
    }


}
