import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import '@fengyuanchen/datepicker';

import {SharedHttpService} from '../services/SharedHttp.service';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-datewise-sales-journal',
    templateUrl: './datewise-sales-journal.component.html',
    styles: []
})
export class DatewiseSalesJournalComponent implements OnInit {
    columns = [];
    public rows;
    public DateWiseSalesJournal;
    locationId
    loadingIndicator = true;
    private subscription: Subscription;

    constructor(private printService: PrintService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {

        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'datewise-sales-journal') {
                this.locationId = parcel.locationId;

                this.DateWiseSalesJournal = this.rows = parcel.DateWiseSalesJournal;
                this.loadingIndicator = false;
                for (const key in this.DateWiseSalesJournal[0]) {
                    if (this.DateWiseSalesJournal[0].hasOwnProperty(key))
                        this.columns.push({Key: key, IncludeInPrint: true});

                }
            }
        });
    }

    columnSelectionChanged(event, i) {
        console.log('index', i)
        this.columns[i].IncludeInPrint = !this.columns[i].IncludeInPrint;
    }

    printReport() {
        console.log('cols', this.columns)

        let page = this;

        let temp = this.clone(this.rows);

        const mappedRows = temp.map(function (item) {
            delete item['_id'];
            for (const key in item) {
                if (item.hasOwnProperty(key) && !page.columns.find(x => x.Key === key).IncludeInPrint)
                    delete item[key];
            }
            return item;
        });
        this.printService.printReport([mappedRows], 'Date Wise Sales Journal', this.locationId);
    }
    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        this.rows = this.DateWiseSalesJournal;
        // filter our data
        this.rows = this.rows.filter(function (d) {
            return d.Date.toLowerCase().indexOf(val) !== -1 || !val;
        });


    }

    ngOnInit() {
    }


}
