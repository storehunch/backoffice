import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {SharedHttpService} from '../services/SharedHttp.service';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {PrintService} from "../services/print.service";

@Component({
    selector: 'app-cumilative-stock-sales-report',
    templateUrl: './cumilative-stock-sales-report.component.html',
    styles: []
})
export class CumilativeStockSalesReportComponent implements OnInit {
    columns = [];

    public CumilativeStockReport;
    rows;
    loadingIndicator = true;
    locationId;
    private subscription: Subscription;

    constructor(private printService: PrintService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService) {

        this.subscription = this.transporter.receive().subscribe(parcel => {

            if (parcel.type === 'cumilative-stock-report') {
                this.locationId = parcel.locationId;

                this.rows = this.CumilativeStockReport = parcel.CumilativeStockReport;
                this.loadingIndicator = false;
                for (const key in this.CumilativeStockReport[0]) {
                    if (this.CumilativeStockReport[0].hasOwnProperty(key))
                        this.columns.push({Key: key, IncludeInPrint: true});

                }
            }


        });
    }

    // getProductAttribute(id) {
    //     this.httpService.Get('/productattributes/' + id).subscribe(data => {
    //         console.log(data);
    //     }, err => {
    //
    //     });
    // }
    columnSelectionChanged(event, i) {
        console.log('index', i)
        this.columns[i].IncludeInPrint = !this.columns[i].IncludeInPrint;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        this.rows = this.CumilativeStockReport;
        // filter our data
        this.rows = this.rows.filter(function (d) {
            return d.Name.toLowerCase().indexOf(val) !== -1 || !val || (d.Barcode && d.Barcode.toLowerCase().indexOf(val) !== -1);
        });


    }

    printReport() {
        let page=this;
        let temp = this.clone(this.rows);
        const mappedRows = temp.map(function (item) {
            delete item['_id'];
            for (const key in item) {
                if (item.hasOwnProperty(key) && !page.columns.find(x => x.Key === key).IncludeInPrint)
                    delete item[key];
            }
            return item;
        });
        this.printService.printReport([mappedRows], 'Stock Report', this.locationId);
    }

    ngOnInit() {
    }
    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }

}
