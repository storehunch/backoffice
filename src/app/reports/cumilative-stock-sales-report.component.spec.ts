import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CumilativeStockSalesReportComponent } from './cumilative-stock-sales-report.component';

describe('CumilativeStockSalesReportComponent', () => {
  let component: CumilativeStockSalesReportComponent;
  let fixture: ComponentFixture<CumilativeStockSalesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CumilativeStockSalesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CumilativeStockSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
