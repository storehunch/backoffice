import { SharedHttpService } from './../services/SharedHttp.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../models/User';
import { TransporterService } from '../services/transporter.service';
import { Subscription } from 'rxjs/Subscription';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';
import { DexieService } from '../services/dexie.service';

@Component({
    selector: 'app-supplier-listing',
    templateUrl: './supplier-listing.component.html',
    styles: []
})
export class SupplierListingComponent implements OnInit {


    public suppliers = [];
    public currentUser: User;
    public isOnline = true;
    private subscription: Subscription;
    public locationId: string;
    loadingIndicator = true;
    filterArray = [true, false, false, false, false];
    public searchText = '';
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private transporter: TransporterService, private dexieService: DexieService) {
    }
    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'supplier-listing') {

                this.dexieGetSuppliers();
                this.getUpdatedSuppliers();

            }

        });
        this.dexieGetSuppliers();
        this.getUpdatedSuppliers();
    }

    getRowHeight(row) {
        return row.height;
    }
    // todo: dexieGetSuppliers() !
    dexieGetSuppliers() {
        this.dexieService['Supplier'].where({ Company: this.currentUser.Company }).toArray()
            .then(data => {
                this.suppliers = data;
                for (let index = 0; index < this.suppliers.length; index++) {
                    const supplier = this.suppliers[index];
                    this.getPayableForSupplier(supplier, index);
                }
                this.loadingIndicator = false;
            }).catch(err => {
                this.suppliers = [];
            });
    }

    getUpdatedSuppliers() {
        let lastUpdated;
        this.dexieService['Flag'].where({ Name: 'Supplier' }).toArray().then(data => {
            lastUpdated = data[0];
            this.getSuppliers(lastUpdated['Value']);
        }).catch(err => {
            this.getSuppliers(false);
        });
    }
    getSuppliers(updatedAt) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }
        this.httpService.Get('/suppliers?company=' + this.currentUser.Company + queryPart)
            .subscribe(
            res => {
                if (res['status'] !== 204) {

                    this.dexieService['Flag'].put({ Name: 'Supplier', Value: new Date().toJSON() }).catch(flagErr => {
                        console.log('Failed to put Supplier flag ' + flagErr);
                    });
                    this.dexieService['Supplier'].bulkPut(res['body']['data']).then(data => {
                        this.dexieGetSuppliers();
                    }).catch(dexieErr => {
                        console.log('Failed to put resulted Supplier: ' + dexieErr);
                    });
                }
            },
            err => {
                if (this.isOnline) {
                    console.log('Unexpected error');
                }
            }
            );
    }
    getPayableForSupplier(row, index) {

        const page = this;
        this.httpService.Get('/receipts?company=' + this.currentUser.Company + '&store=' + this.locationId + '&supplier=' + row['_id'])
            .subscribe(
            res => {
                if (res['status'] !== '204') {
                    const receipts = res['body']['data'];
                    page.suppliers[index]['Payable'] = 0;
                    if (receipts) {
                        receipts.forEach(receipt => {

                            if (receipt['PaymentMethod'] === 'oncredit') {

                                if (receipt['Payable']) {
                                   page.suppliers[index]['Payable'] += receipt['Payable'];

                                } else {
                                    console.log('no payable for supplier ' + row['Name']);
                                }
                            }

                            if (receipt['TransactionType'] === 'payback') {


                                if (receipt['AmountPaid']) {
                                    page.suppliers[index]['Payable'] -= receipt['AmountPaid'];
                                }


                            }


                        });
                    }


                }

            },
            err => {
                console.log('err');
            }
            );

    }

}
