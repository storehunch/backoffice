import {DexieService} from './../../services/dexie.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from './../../models/User';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {SharedHttpService} from './../../services/SharedHttp.service';
import {TransporterService} from './../../services/transporter.service';
import {Component, OnInit} from '@angular/core';
import {OnDestroy} from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-supplier-payback',
    templateUrl: './supplier-payback.component.html',
    styles: []
})
export class SupplierPaybackComponent implements OnInit, OnDestroy {
    locationId;
    public currentUser: User;
    public isOnline = true;
    public ModalState = false;
    public FormState = '';
    public dataForm: FormGroup;
    public FormErr: String = '';
    suppliers = [];
    accounts = [];
    loadingIndicator = false;
    private subscription: Subscription;

    constructor(private transporter: TransporterService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute, private fb: FormBuilder, private dexieService: DexieService) {
    }

    ngOnInit() {
        const page = this;

        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'supplier-payback') {
                this.dexieGetSuppliers();
                this.getAccounts();
                if (parcel.data === 'start') {
                    $('body').removeClass('overflow-hidden');
                    this.ToggleModal();

                }
            }
        });
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });
        this.dataForm = this.fb.group({
            Supplier: ['', Validators.required],
            AmountPaid: ['', Validators.required],
            Company: [this.currentUser['Company']],
            SalesPerson: [this.currentUser._id],
            Store: [this.locationId],
            TransactionType: 'payback',
            Date: [new Date()],
            PaidFrom: ['', Validators.required]
        });


    }

    resetForm() {
        this.dataForm.reset({
            Supplier: '',
            AmountPaid: '',
            Company: this.currentUser['Company'],
            SalesPerson: this.currentUser._id,
            Store: this.locationId,
            TransactionType: 'payback',
            Date: new Date(),
            PaidFrom: ['', Validators.required]
        });
        this.dataForm.get('Supplier').setValidators([Validators.required]);
        this.dataForm.get('AmountPaid').setValidators(Validators.required);
        this.dataForm.updateValueAndValidity();
    }

    submit() {

        if (this.dataForm.valid) {
            this.FormState = 'loading';
            this.PostPayback();
        } else {
            console.log(this.dataForm.value);
            console.log('not valid');
            Object.keys(this.dataForm.controls).forEach(field => {
                const control = this.dataForm.get(field);
                control.markAsDirty({onlySelf: true});
            });
        }
    }

    dexieGetSuppliers() {
        this.dexieService['Supplier'].where({Company: this.currentUser.Company}).toArray()
            .then(data => {
                this.suppliers = data;
                for (let index = 0; index < this.suppliers.length; index++) {
                    const supplier = this.suppliers[index];
                    this.getPayableForSupplier(supplier, index);
                }

            }).catch(err => {
            this.suppliers = [];
        });
    }

    getPayableForSupplier(row, index) {

        const page = this;
        this.httpService.Get('/receipts?company=' + this.currentUser.Company + '&store=' + this.locationId + '&supplier=' + row['_id'])
            .subscribe(
                res => {
                    if (res['status'] !== '204') {
                        const receipts = res['body']['data'];
                        page.suppliers[index]['Payable'] = 0;
                        if (receipts) {
                            receipts.forEach(receipt => {

                                if (receipt['PaymentMethod'] === 'oncredit') {

                                    if (receipt['Payable']) {

                                        console.log(index);
                                        page.suppliers[index]['Payable'] += receipt['Payable'];

                                    } else {
                                        console.log('--no payable for supplier ' + row['Name']);
                                    }
                                }
                                if (receipt['TransactionType'] === 'payback') {


                                    if (receipt['AmountPaid']) {
                                        page.suppliers[index]['Payable'] -= receipt['AmountPaid'];
                                    }


                                }


                            });
                        }


                    }

                },
                err => {
                    console.log('err');
                }
            );

    }


    PostPayback() {
        if (this.dataForm.value.AmountPaid < 0 || this.dataForm.value.AmountPaid > (this.suppliers.find(x => x._id === this.dataForm.value.Supplier).Payable)) {

            this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>Amount</strong>  Invalid</div>`;
            return;
        }

        this.httpService.Post('/receipts', this.dataForm.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.resetForm();
                    this.transport({type: 'supplier-payback', data: 'success'});
                    this.transport({type: 'supplier-listing', data: 'start'});
                    this.ToggleModal();
                },
                err => {
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }
                        }
                    }
                }
            );
    }

    /*
    * execute these line on expense success
    * this.transport({type: 'expense', data: 'success'});
    * this.ToggleModal();
    * */


    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


    getAccounts() {
        this.httpService.Get('/accounts?company=' + this.currentUser.Company )
            .subscribe(
                res => {
                    if (res['status'] !== 204) {
                        this.accounts = res['body']['data'];
                        console.log('accounts', this.accounts);
                    } else {
                        this.accounts = [];
                    }
                    this.loadingIndicator = false;
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error');
                    }
                    this.loadingIndicator = false;
                }
            );

    }

}
