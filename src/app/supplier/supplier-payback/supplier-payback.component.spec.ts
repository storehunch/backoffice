import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierPaybackComponent } from './supplier-payback.component';

describe('SupplierPaybackComponent', () => {
  let component: SupplierPaybackComponent;
  let fixture: ComponentFixture<SupplierPaybackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierPaybackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierPaybackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
