import {Component, OnInit, OnDestroy} from '@angular/core';
import {User} from '../models/User';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import {DexieService} from '../services/dexie.service';
import {SharedHttpService} from '../services/SharedHttp.service';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import * as $ from 'jquery';

function emailOrEmpty(control: AbstractControl): ValidationErrors | null {
    return control.value === '' ? null : Validators.email(control);
}

@Component({
    selector: 'app-supplier',
    templateUrl: './supplier.component.html',
    styles: []
})
export class SupplierComponent implements OnInit, OnDestroy {

    FormErr;
    public title = 'Suppliers';
    public currentUser: User;
    public isOnline = true;
    public ModalState = false;
    public taxonomies = [];
    public formGroup: FormGroup;
    public FormState = '';
    private subscription: Subscription;

    constructor(private fb: FormBuilder, private httpService: SharedHttpService, private transporter: TransporterService, private dexieService: DexieService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'supplier' && parcel.data === 'start') {
                this.ToggleModal();
            }
        });
    }


    /*
   * execute these line on expense success
   * this.transport({type: 'supplier', data: 'success'});
   * this.ToggleModal();
   * */


    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.formGroup = this.fb.group({
            Name: ['', Validators.required],
            Phone: ['', Validators.required],
            Company: [this.currentUser.Company, Validators.required],
            Address: [''],
            Email: ['', emailOrEmpty]
        });
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


    submit() {
        if (this.formGroup.valid) {
            this.FormState = 'loading';
            this.Post();
        } else {
            Object.keys(this.formGroup.controls).forEach(field => {
                const control = this.formGroup.get(field);
                control.markAsDirty({ onlySelf: true });
            });
        }
    }

    Post() {
        this.httpService.Post('/suppliers', this.formGroup.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.transport({type: 'supplier', data: 'success'});
                    this.formGroup.reset({
                        Name: '',
                        Phone: '',
                        Address: '',
                        Email: '',
                        Company: this.currentUser.Company,
                    });
                    this.ToggleModal();
                },
                err  => {
                    if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }
                        }
                    }
                }
            );
    }


}
