import {Component, OnInit, OnDestroy} from '@angular/core';
import {User} from '../models/User';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import * as XLSX from 'xlsx';
import {DexieService} from '../services/dexie.service';
import {SharedHttpService} from '../services/SharedHttp.service';
import * as $ from 'jquery';

@Component({
    selector: 'app-excel-import',
    templateUrl: './excel-import.component.html',
    styles: []
})
export class ExcelImportComponent implements OnInit, OnDestroy {

    public currentUser: User;
    public isOnline = true;
    public ModalState = false;
    public excelData: any;
    public taxonomies = [];
    public FormState = '';
    public newCategories = [];
    public newTags = [];
    public newTaxonomies = [];
    public progress = {status: 0, text: '0%'};
    private subscription: Subscription;

    constructor(private httpService: SharedHttpService, private transporter: TransporterService, private dexieService: DexieService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
            if (parcel.type === 'import') {
                if (parcel.data === 'start') {
                    this.ToggleModal();
                }
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    importExcelData(evt) {
        const target: DataTransfer = <DataTransfer>(evt.target);
        let okayToImport = true;
        if (target.files.length !== 1) {
            alert('Cannot use multiple files');
            okayToImport = false;
        }
        if (target.files[0].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            alert('Only .xlsx file is acceptable.');
            okayToImport = false;
        }
        const reader: FileReader = new FileReader();
        if (okayToImport) {
            reader.onload = (e: any) => {
                /* read workbook */
                this.FormState = 'loading';
                this.progress = {
                    status: 2,
                    text: '2%'
                };
                const bstr: string = e.target.result;
                const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

                const wsname: string = wb.SheetNames[0];
                const ws: XLSX.WorkSheet = wb.Sheets[wsname];

                this.excelData = XLSX.utils.sheet_to_json(ws, {header: 2});

                for (let p = 0; p < this.excelData.length; p++) {
                    this.excelData[p].Company = this.currentUser.Company;
                    this.excelData[p].Approved = true;
                    if (this.excelData[p].Barcode && this.excelData[p].Barcode.trim().length) {
                        this.excelData[p].CompanyBarcode = this.currentUser.Company + this.excelData[p].Barcode;
                    }
                    this.excelData[p].Approved = true;
                    /*this.excelData[p].Approved = this.currentUser.AccessLevel === 'SuperAdmin';*/
                    if (typeof (this.excelData[p].Tags) !== 'undefined' && this.excelData[p].Tags.length > 2) {
                        this.excelData[p].Tags = this.excelData[p].Tags.replace(/\s/g, '').split(',');
                        const tempTag = this.excelData[p].Tags;
                        for (let t = 0; t < tempTag.length; t++) {
                            if (this.newTags.lastIndexOf(tempTag[t]) === -1) {
                                this.newTags.push(tempTag[t]);
                            }
                        }
                    }
                    if (typeof (this.excelData[p].Category) !== 'undefined' && this.excelData[p].Category.length > 2) {
                        this.excelData[p].Category = this.excelData[p].Category.replace(/\s/g, '').split(',');
                        const tempCat = this.excelData[p].Category;
                        for (let t = 0; t < tempCat.length; t++) {
                            if (this.newCategories.lastIndexOf(tempCat[t]) === -1) {
                                this.newCategories.push(tempCat[t]);
                            }
                        }
                    }
                }


                this.newTags.forEach(tag => {
                    this.newTaxonomies.push({Name: tag, IsCategory: false, Company: this.currentUser.Company});
                });

                this.newCategories.forEach(tag => {
                    this.newTaxonomies.push({Name: tag, IsCategory: true, Company: this.currentUser.Company});
                });

                this.progress = {
                    status: 10,
                    text: '10%'
                };
                this.httpService.Post('/taxonomies/bulk', this.newTaxonomies)
                    .subscribe(
                        blkRes => {
                            this.progress = {
                                status: 30,
                                text: '30%'
                            };
                            this.httpService.Get('/taxonomies?company=' + this.currentUser['Company'])
                                .subscribe(
                                    res => {
                                        this.progress = {
                                            status: 60,
                                            text: '60%'
                                        };
                                        this.taxonomies = res['body']['data'] ? res['body']['data'] : [];
                                        /*=======Replace Tag Names with ID=============*/
                                        this.progress = {
                                            status: 70,
                                            text: '70%'
                                        };
                                        for (let p = 0; p < this.excelData.length; p++) {
                                            if (this.excelData[p].Tags) {
                                                for (let t = 0; t < this.excelData[p].Tags.length; t++) {
                                                    for (let src = 0; src < this.taxonomies.length; src++) {
                                                        if (this.taxonomies[src].Name === this.excelData[p].Tags[t]) {
                                                            this.excelData[p].Tags[t] = this.taxonomies[src]._id;
                                                        }
                                                    }
                                                }
                                            }
                                            if (this.excelData[p].Category) {
                                                for (let t = 0; t < this.excelData[p].Category.length; t++) {
                                                    for (let src = 0; src < this.taxonomies.length; src++) {
                                                        if (this.taxonomies[src].Name === this.excelData[p].Category[t]) {
                                                            this.excelData[p].Category[t] = this.taxonomies[src]._id;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        this.progress = {
                                            status: 80,
                                            text: '80%'
                                        };

                                        this.httpService.Post('/products/bulk', this.excelData)
                                            .subscribe(
                                                blkProdPost => {
                                                    this.progress = {
                                                        status: 99,
                                                        text: '99%'
                                                    };
                                                    setTimeout(() => {
                                                        this.progress = {
                                                            status: 100,
                                                            text: 'Done'
                                                        };
                                                    }, 1000);
                                                    setTimeout(() => {
                                                        this.ToggleModal();
                                                        this.FormState = '';
                                                        this.transport({type: 'import', data: 'success'});
                                                        location.reload();
                                                    }, 2000);
                                                },
                                                bldProdErr => {
                                                    if (this.isOnline) {
                                                        console.log(bldProdErr);
                                                    }
                                                });

                                        /*============================================*/
                                    },
                                    err => {
                                        if (this.isOnline) {
                                            console.log('Unexpected error with status ' + err.status);
                                        }
                                    }
                                );
                        },
                        err => {
                            if (this.isOnline) {
                                console.log(err);
                            }
                        }
                    );


            };
            reader.readAsBinaryString(target.files[0]);
        }
    }

    transport(packet: any): void {
        this.transporter.transmit(packet);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
