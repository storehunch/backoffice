import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl} from '@angular/forms'
import { Router } from '@angular/router';

import { SharedHttpService } from '../services/SharedHttp.service';
import { User } from '../models/User';

function emailOrEmpty(control: AbstractControl): ValidationErrors | null {
    return control.value === '' ? null : Validators.email(control);
}

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styles: []
})
export class AddLocationComponent implements OnInit {
    @Input() StoreType = 'Outlet';
    @Output() StoreCreated = new EventEmitter();
    form_group: FormGroup;
    FormErr: String = '';
    FormState: String = '';
    public CurrentUser: User = JSON.parse(localStorage.getItem('user'));

    constructor (private fb: FormBuilder, private router: Router, private httpService: SharedHttpService) {}

    ngOnInit() {
        this.form_group = this.fb.group({
            Name: ['', Validators.required],
            Phone: [''],
            StoreType: ['Outlet', Validators.required],
            Email: ['', emailOrEmpty],
            Street: ['', Validators.required],
            City: ['', Validators.required],
            Company: [this.CurrentUser.Company]
        });
    }

    private CreateStore() {
        if (this.form_group.value.Email.replace(/ /g, '') === '') {
            delete this.form_group.value.Email;
        }
        console.log(this.form_group.value);
        this.httpService.Post('/stores', this.form_group.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.router.navigateByUrl('/');
                },
                err  => {
                    if (err.status === 401) {
                        console.log('Not Authorized');
                    } else if (err.error) {
                        const data = err.error.errors;
                        for (const key of Object.keys(data)) {
                            if (data[key].kind === 'required') {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
                            } else {
                                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
                            }

                        }
                    }
                }
            );
    }


    submit() {
        if (this.form_group.valid) {
            this.FormState = 'loading';
            this.CreateStore();
        } else {
            Object.keys(this.form_group.controls).forEach(field => {
                const control = this.form_group.get(field);
                control.markAsDirty({ onlySelf: true });
            });
        }
    }

}
