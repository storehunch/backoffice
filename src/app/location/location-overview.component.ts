import {Component, OnInit} from '@angular/core';
import {User} from '../models/User';
import {SharedHttpService} from '../services/SharedHttp.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {PrintService} from '../services/print.service';

@Component({
    selector: 'app-location-overview',
    templateUrl: './location-overview.component.html',
    styles: []
})
export class LocationOverviewComponent implements OnInit {

    public CurrentUser: User;
    Totals;
    IncomingStock = [];
    public IsReady = false;
    public locationId: string;
    public date_from: Date;
    transactions = []
    res;
    private subscription: Subscription;

    constructor(private transporter: TransporterService, private httpService: SharedHttpService, private route: ActivatedRoute, private router: Router, private  printService: PrintService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'get-shipment') {
                this.GetIncomingStock();
            }
        });

    }

    ngOnInit() {
        this.date_from = new Date();
        console.log(this.date_from);
        this.CurrentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.route.snapshot.params['id'];

        this.GetIncomingStock();
        this.getReports();

    }

    getReports() {
        if (this.date_from) {
            console.log('gaga');
            let today = new Date();

            this.httpService.Get('/reports?company=' + this.CurrentUser.Company + '&stores=' + this.locationId + '&date_from=' + (today.getMonth() + 1) + '/' + (today.getDay() + 2) + '/' + today.getFullYear() + '&date_to=' + (today.getMonth() + 1) + '/' + (today.getDay() + 3) + '/' + today.getFullYear())
                .subscribe(
                    res => {
                        console.log(res);
                        this.res = res['body'];
                        this.Totals = res['body']['totals'];
                        this.transactions = res['body']['transactions'];
                    },
                    err => {
                        console.log(err);
                    }
                );


        }
    }


    showShipment(receipt) {
        this.transporter.transmit({type: 'shipment', data: receipt});
    }

    showNewProducts(products) {
        this.transporter.transmit({type: 'new-products', data: products});
    }

    GetIncomingStock() {
        this.httpService.Get(('/receipts?TransactionStatus=pending&TransactionType=transfer&ShippedTo=' + this.locationId + '&company=' + this.CurrentUser.Company).toLowerCase())
            .subscribe(
                res => {
                    if (res['body']['status'] !== 204) {
                        this.IncomingStock = res['body']['data'];
                    } else {
                        this.IncomingStock = [];
                    }
                },
                err => {
                    console.log(err);
                    this.IncomingStock = [];
                }
            );
    }

    printReport() {
        let temp = [];
        for (const key in this.Totals) {
            if (this.Totals.hasOwnProperty(key)) {
                temp.push({Head: key, Amount: this.Totals[key]});

            }

        }


        let tempTransactions = this.clone(this.transactions);
        let page = this;
        const mappedRows = tempTransactions.map(function (item) {

            item['Transaction'] = '';
            if (item['From'] && item['To']) {
                item['Transaction'] = item['From'].Title + ' -> ' + item['To'].Title;
            } else {
                item['Transaction'] = '-';

            }
            if (item['From'] && item['Supplier']) {
                item['Transaction'] = item['From'].Title + ' -> ' + item['Supplier'].Name;

            }
            item['Balance'] = item['FromBalance'] ? item['FromBalance'] : 'N/A';
            const mDate = item['createdAt'].split('T')[0].split('-')[2] + '-' + item['createdAt'].split('T')[0].split('-')[1] + '-' + item['createdAt'].split('T')[0].split('-')[0];
            // const mTime = item['createdAt'].split('T')[1].split(':')[2] + '-' + item['createdAt'].split('T')[1].split(':')[1] + '-' + item['createdAt'].split('T')[1].split(':')[0];

            item['Date'] = mDate;
            // item['Debit'] = item.To !== undefined && item.To._id === page.locationId ? item['Amount'] : ' - ';
            // item['Credit'] = item.From && item.From._id === page.locationId ? item['Amount'] : ' - ';
            // item['Date'] = mDate;

            delete item['_id'];
            delete item['Supplier'];
            delete item['updatedAt'];
            delete item['createdAt'];
            delete item['Company'];
            delete item['__v'];
            delete item['User'];
            delete item['From'];
            delete item['To'];
            delete item['ToBalance'];
            delete item['FromBalance'];
            return item;
        });


        let tempExpenses = this.clone(this.res.expenses).map(item => {
            const mDate = item['Date'].split('T')[0].split('-')[2] + '-' + item['Date'].split('T')[0].split('-')[1] + '-' + item['Date'].split('T')[0].split('-')[0];
            item['Date'] = mDate;
            delete item['_id'];
            delete item['Company'];
            delete item['User'];
            delete item['Store'];
            delete item['ExpenseType'];
            delete item['createdAt'];
            delete item['updatedAt'];
            delete item['__v'];

            return item;
        });


        this.printService.printReport([temp, tempTransactions, tempExpenses, this.clone(this.res.categoryWiseSalesReport).map(item => {
            delete item['_id'];
            return item;
        })], 'Day Closing Report', this.locationId, '', ['', tempTransactions.length && tempTransactions.length > 0 ? 'Transactions' : '', tempExpenses.length && tempExpenses.length > 0 ? 'Expenses' : '', this.res.categoryWiseSalesReport.length && this.res.categoryWiseSalesReport.length > 0 ? 'Category Wise Sales Report' : '']);
    }

    clone(source) {
        if (Object.prototype.toString.call(source) === '[object Array]') {
            const clone = [];
            for (let i = 0; i < source.length; i++) {
                clone[i] = this.clone(source[i]);
            }
            return clone;
        } else if (typeof (source) === 'object') {
            const clone = {};
            for (const prop in source) {
                if (source.hasOwnProperty(prop)) {
                    clone[prop] = this.clone(source[prop]);
                }
            }
            return clone;
        } else {
            return source;
        }
    }


}


