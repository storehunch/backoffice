import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {SharedHttpService} from '../services/SharedHttp.service';
import {User} from '../models/User';
import * as $ from 'jquery';

function emailOrEmpty(control: AbstractControl): ValidationErrors | null {
  return control.value === '' ? null : Validators.email(control);
}

@Component({
  selector: 'app-location-detail-setting',
  templateUrl: './location-detail-setting.component.html',
  styles: []
})
export class LocationDetailSettingComponent implements OnInit {

  public title = 'Location Settings';
  public LocationId: string;
  form_group: FormGroup;
  FormErr: String = '';
  FormState: String = '';
  public ModalState = false;
  public CurrentStore: any;
  public CurrentUser: User = JSON.parse(localStorage.getItem('user'));

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private fb: FormBuilder, private httpService: SharedHttpService) {
  }

  ngOnInit() {
    $('body').removeClass('overflow-hidden');
    $('.off-canvas').removeClass('active');
    this.activatedRoute.params.subscribe(params => {
      this.LocationId = params['id'];
    });
    this.form_group = this.fb.group({
      Name: ['', Validators.required],
      Phone: [''],
      Street: ['', Validators.required],
      Email: ['', emailOrEmpty],
      City: ['', Validators.required],
      StoreType: ['', Validators.required],
      ReceiptPageSize: ['', Validators.required]
    });
    this.GetStore();
  }

  GetStore() {
    this.httpService.Get('/stores/' + this.LocationId)
      .subscribe(
        res => {
          this.CurrentStore = res['body'];
          this.form_group.setValue({
            Name: this.CurrentStore.Name ? this.CurrentStore.Name : '',
            Phone: this.CurrentStore.Phone ? this.CurrentStore.Phone : '',
            Email: this.CurrentStore.Email ? this.CurrentStore.Email : '',
            Street: this.CurrentStore.Street ? this.CurrentStore.Street : '',
            StoreType: this.CurrentStore.StoreType ? this.CurrentStore.StoreType : '',
            ReceiptPageSize: this.CurrentStore.ReceiptPageSize ? this.CurrentStore.ReceiptPageSize : 'mm80',
            City: this.CurrentStore.City ? this.CurrentStore.City : '',
          });
        },
        err => {
          console.log('Unexpected error, Please contact support.');
        }
      );
  }

  private PatchStore() {
    if (this.form_group.value.Email && this.form_group.value.Email.replace(/ /g, '') === '') {
      delete this.form_group.value.Email;
    }
    this.httpService.Patch('/stores/' + this.LocationId, this.form_group.value)
      .subscribe(
        data => {
          this.FormState = '';
          this.router.navigateByUrl('/locations/' + this.LocationId);
        },
        err => {
          if (err.status === 401) {
            console.log('Not Authorized');
          } else if (err.error) {
            const data = err.error.errors;
            for (const key of Object.keys(data)) {
              if (data[key].kind === 'required') {
                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  is required.</div>`;
              } else {
                this.FormErr += `<div class="alert alert-danger alert-dismissible fade show mt-10" role="alert"><strong>${key}</strong>  already exists.</div>`;
              }

            }
          }
        }
      );
  }

  ToggleModal() {
    $('body').toggleClass('overflow-hidden');
    return this.ModalState = !this.ModalState;
  }


  submit() {
    if (this.form_group.valid) {
      this.FormState = 'loading';
      this.PatchStore();
    } else {
      Object.keys(this.form_group.controls).forEach(field => { // {1}
        const control = this.form_group.get(field);            // {2}
        control.markAsDirty({onlySelf: true});       // {3}
      });
    }
    this.PatchStore();
  }
}
