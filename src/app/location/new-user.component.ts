import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html',
    styles: []
})
export class NewUserComponent implements OnInit {
    public title = 'Create  User';
    public LocationId: string;
    constructor(private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.LocationId = params['id'];
        });
    }


}
