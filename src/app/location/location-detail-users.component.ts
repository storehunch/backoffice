import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {SharedHttpService} from '../services/SharedHttp.service';

@Component({
  selector: 'app-location-detail-users',
  templateUrl: './location-detail-users.component.html',
  styles: []
})
export class LocationDetailUsersComponent implements OnInit {
    public title = 'Location Users';
    public LocationId = this.activatedRoute.snapshot.params['id'];
  constructor(private activatedRoute: ActivatedRoute, private httpService: SharedHttpService) { }

  ngOnInit() {
  }


}
