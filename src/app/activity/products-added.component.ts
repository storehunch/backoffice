import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-products-added',
  templateUrl: './products-added.component.html',
  styles: []
})
export class ProductsAddedComponent implements OnInit {

    public products = [];
    public ModalState = false;
    private subscription: Subscription;

    constructor(private transporter: TransporterService) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'new-products') {
                this.products = parcel.data;
                this.ToggleModal();
            }
        });
    }

    ngOnInit() {
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }
}
