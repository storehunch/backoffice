import {User} from './../models/User';
import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {TransporterService} from '../services/transporter.service';
import * as $ from 'jquery';
import {SharedHttpService} from '../services/SharedHttp.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-incoming-shipment',
    templateUrl: './incoming-shipment.component.html',
    styles: []
})
export class IncomingShipmentComponent implements OnInit {

    public shipment = {};
    public ModalState = false;
    private subscription: Subscription;
    public currentUser: User;
    locationId;

    constructor(private transporter: TransporterService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'shipment') {
                this.shipment = parcel.data;
                this.ToggleModal();
            }
        });
    }

    acceptTransfer() {
        const receipt = {
            ReferenceReceipt: this.shipment['_id'],
            TransactionType: 'receive',
            Store: this.locationId,
            Company: this.currentUser.Company,
            SalesPerson: this.currentUser._id,
            Products: this.shipment['Products'],
            Date: new Date()

        };
        this.httpService.Post('/receipts', receipt).subscribe(data => {
            console.log(data);
            this.transporter.transmit({type: 'get-shipment', data: true});
            this.ToggleModal();
        }, err => {
            console.log(err);
        });

    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.locationId = this.currentUser.Store ? this.currentUser.Store : this.activatedRoute.snapshot.params['id'];
        this.activatedRoute.params.subscribe(params => {
            this.locationId = params['id'];
        });
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

}
