import { ActivatedRoute } from '@angular/router';
import { SharedHttpService } from './../../services/SharedHttp.service';
import { TransporterService } from './../../services/transporter.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-day-opening',
  templateUrl: './day-opening.component.html',
  styles: []
})
export class DayOpeningComponent implements OnInit {

  constructor(private transporter: TransporterService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute){ }

  ngOnInit() {
  }

}
