import { ActivatedRoute } from '@angular/router';
import { SharedHttpService } from './../../services/SharedHttp.service';
import { TransporterService } from './../../services/transporter.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-day-closing',
  templateUrl: './day-closing.component.html',
  styles: []
})
export class DayClosingComponent implements OnInit {

  constructor(private transporter: TransporterService, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
