import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {SharedHttpService} from '../services/SharedHttp.service';
import {DayClosing, User} from '../models/AllModels';


@Component({
    selector: 'app-day',
    templateUrl: './day.component.html',
    styles: []
})
export class DayComponent implements OnInit {
    public title = 'Day Closings';
    public Days: DayClosing[] = [];
    public LocationId: string;
    public IsReady = false;
    public CurrentUser: User = JSON.parse(localStorage.getItem('user'));

    constructor(private router: Router, private httpService: SharedHttpService, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.LocationId = params['id'];
            this.IsReady = false;
            this.GetDays();
        });
    }

    GetDays() {
        this.httpService.Get('/dayclosings?company=' + this.CurrentUser.Company + '&store=' + this.LocationId)
            .subscribe(
                res => {
                    this.IsReady = true;
                    this.Days = res['body']['data'];
                },
                err => {
                    console.log('Unexpected error, Please contact support.');
                }
            );
    }

}
