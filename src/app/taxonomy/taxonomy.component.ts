import {Component, OnInit} from '@angular/core';
import {SharedHttpService} from '../services/SharedHttp.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {TransporterService} from '../services/transporter.service';
import {Subscription} from 'rxjs/Subscription';
import {DexieService} from '../services/dexie.service';
import * as $ from 'jquery';

@Component({
    selector: 'app-taxonomy',
    templateUrl: './taxonomy.component.html',
    styles: []
})
export class TaxonomyComponent implements OnInit {

    public title = 'Tags & Categories';
    public currentUser = [];
    public tags: any[] = [];
    public categories: any[] = [];
    public formGroup: FormGroup;
    public IsParent = false;
    public ModalState = false;
    public FormState = '';
    public isOnline = true;
    private subscription: Subscription;

    constructor(private dexieService: DexieService, private transporter: TransporterService, private httpService: SharedHttpService, private fb: FormBuilder) {
        this.subscription = this.transporter.receive().subscribe(parcel => {
            if (parcel.type === 'connection') {
                this.isOnline = parcel.data;
            }
        });
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.dexieGetTaxonomies();
        this.getUpdatedTaxonomies();
        this.formGroup = this.fb.group({
            Name: ['', Validators.required],
            IsCategory: ['', Validators.required],
            Company: [this.currentUser['Company']],
            Parent: [''],
        });
        this.formGroup.get('IsCategory').valueChanges.subscribe(value => {
            this.IsParent = value === 'true';
            const parentControl = this.formGroup.get('Parent');
            parentControl.updateValueAndValidity();
        });
    }

  dexieGetTaxonomies() {
    this.dexieService['Taxonomy'].where({Company: this.currentUser['Company']}).toArray().then(data => {
      this.tags = data.filter(obj => {
          if (!obj.IsCategory) {
            return obj;
          }
      });
      this.categories = data.filter(obj => {
          if (obj.IsCategory) {
            return obj;
          }
      });
    }).catch(err => {
      console.log(err);
      console.log('err in taxonomies record.');
    });
  }

    getUpdatedTaxonomies() {
        let lastUpdated;
        if (this.isOnline) {
            this.dexieService['Flag'].where({Name: 'Taxonomy'}).toArray().then(data => {
                lastUpdated = data[0];
                this.GetTaxonomies(lastUpdated['Value']);
            }).catch(err => {
                this.GetTaxonomies(false);
            });
        }
    }

    GetTaxonomies(updatedAt: any) {
        let queryPart = '';
        if (updatedAt) {
            queryPart = '&updatedAt=' + updatedAt;
        }
        console.log('GetTaxonomies()');
        this.httpService.Get('/taxonomies?company=' + this.currentUser['Company'] + queryPart)
            .subscribe(
                res => {
                    if ( res['status'] !== 204) {
                        this.dexieService['Taxonomy'].bulkPut(res['body']['data']).then( data => {
                            console.log('Taxonomies synced');
                            this.dexieService['Flag'].put({Name: 'Taxonomy', Value: new Date().toJSON()}).catch(flagErr => {
                                console.log('Failed to put Taxonomy flag ' + flagErr);
                            });
                            this.dexieGetTaxonomies();
                        }).catch(syncErr => {
                            console.log('Failed to put resulted company: ' + syncErr);
                        });
                    }else {
                        console.log('Taxonomies are up-to-date');
                    }
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error with status ' + err.status);
                    }
                }
            );
    }

    ToggleModal() {
        $('body').toggleClass('overflow-hidden');
        return this.ModalState = !this.ModalState;
    }

    CreateTaxonomy() {
        if (!this.IsParent || this.formGroup.value.Parent === '') {
            delete this.formGroup.value.Parent;
        }
        this.httpService.Post('/taxonomies', this.formGroup.value)
            .subscribe(
                data => {
                    this.FormState = '';
                    this.ToggleModal();
                    this.getUpdatedTaxonomies();
                },
                err => {
                    if (this.isOnline) {
                        console.log('Unexpected error with status ' + err.status);
                    }
                }
            );
    }

    submit() {
        if (this.formGroup.valid) {
            this.FormState = 'loading';
            this.CreateTaxonomy();
        } else {
            Object.keys(this.formGroup.controls).forEach(field => {
                const control = this.formGroup.get(field);
                control.markAsDirty({onlySelf: true});
            });
        }
    }

}
