import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {Page404Component} from './shared/page404.component';
import {AddLocationComponent} from './location/add-location.component';
import {LocationOverviewComponent} from './location/location-overview.component';
import {LocationDetailSettingComponent} from './location/location-detail-setting.component';
import {LocationDetailUsersComponent} from './location/location-detail-users.component';
import {NewUserComponent} from './location/new-user.component';
import {TaxonomyComponent} from './taxonomy/taxonomy.component';
import {AuthGuardGuard} from './auth.guard';
import {DayComponent} from './day/day.component';
import {ReportsComponent} from './reports/reports.component';
import {ReceiptsComponent} from './receipts/receipts.component';
import {ProductsComponent} from './products/products.component';
import {ReceiptListingComponent} from './receipts/receipt-listing.component';
import {SupplierListingComponent} from './supplier/supplier-listing.component';
import {AccountsListingComponent} from './accounts/accounts-listing/accounts-listing.component';
import {AccountTransactionsComponent} from './accounts/account-transactions/account-transactions.component';
import {SettingsComponent} from './settings/settings.component';
import {CustomersComponent} from './customers/customers.component';
import {AnalyticsComponent} from "./analytics/analytics.component";


const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'locations/:id/accounts', component: AccountsListingComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/new', component: AddLocationComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/receipt', component: ReceiptsComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/receipts', component: ReceiptListingComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/reports', component: ReportsComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/analytics', component: AnalyticsComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id', component: LocationOverviewComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/products', component: ProductsComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/edit', component: LocationDetailSettingComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/days', component: DayComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/users', component: LocationDetailUsersComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/users/new', component: NewUserComponent, canActivate: [AuthGuardGuard]},
    {path: 'locations/:id/suppliers', component: SupplierListingComponent, canActivate: [AuthGuardGuard]},
    {path: 'taxonomies', component: TaxonomyComponent, canActivate: [AuthGuardGuard]},
    {path: 'settings', component: SettingsComponent, canActivate: [AuthGuardGuard]},
    {path: 'signup', component: SignupComponent},
    {path: '', component: DashboardComponent, canActivate: [AuthGuardGuard]},
    {path: 'accounts', component: AccountsListingComponent, canActivate: [AuthGuardGuard]},
    {path: 'accounts/:aid', component: AccountTransactionsComponent, canActivate: [AuthGuardGuard]},
    {path: 'customers', component: CustomersComponent, canActivate: [AuthGuardGuard]},
    {path: '**', component: Page404Component}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
